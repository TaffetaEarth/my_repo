## MY REPO

Репозиторий является результатом учебных проектов за 1 курс и первый семестр 2 курса 

Включает лабортароные работы по Java, небольшие лабораторные на других языках, в том числе Ruby и Python

Также добавлено выполненное ТЗ на Ruby и Grape

# Спецификация по веткам
1. main
   - [Java](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java)
     - [01](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/01) - лабораторная работа курса Java по Bash - переполнение стека
     - [07](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/07%20java) - лабораторная работа курса Java - сортировки, простой калькулятор, Hello World
     - [08](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/08%20java) - лабораторная работа курса Java - партия лабораторных по работе со стандартными потоками, простыми алгоритмами и математическими элементами
     - [09](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/09%20java) - лабораторная работа курса Java - ООП, работа с Gradle, Unit-тесты
     - [13](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/13%20java) - лабораторная работа курса Java - регулярные выражения, работа с javadoc
     - [EasyExamples](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/EasyExamples) - 12 - сборник домашних заданий курса Java
     - [Iterator](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/Iterator/src/ru/sirius/main/program) - лабораторная по реализации интерфейса итератора Java
     - [lessonServlets](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/lessonsServlets) - лабораторная работа по Java и Apache Tomcat, реализация вебапа по учету и созданию сущностей, реализация паттерна Data Transfer Object, взаимодействие с Postgres с использованием Docker
     - [millionmap](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/millionmap) - лабораторная работа по Java - бенчмарк скорости работы алгоритмов для различных классов отображений
     - [serialization](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/serialization) - лабораторная работа по Java - сохранение в файл объекта отображения, извлечение из файла и его обработка, работа с обобщениями
     - [one-connection list](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/one-connection%20list) - лабораторная работа по Java - переопределение методов интерфейса collection для односвязного списка
     - [javafx](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/javafx) - лабораторная работа курса Java - разработка интерфейса для десктопного приложения, аналогичного по функциональности вебапу, реализованному в лабораторной по сервлетам
     - [MatrixReading](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/java/MatrixReading/MatrixSum) - лабораторная работа курса Java - умножение больших по размеру матриц в мультипоточном режиме
   - C
     - [05](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/c/05%20C) - лабораторная работа курса Java по C - сортировки, простой калькулятор
   - Ruby 
     - [06](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/ruby/06%20ruby) - лабораторная работа курса Java по Ruby - сортировки, простой калькулятор
     - [ТЗ на Ruby и Grape](https://github.com/TaffetaEarth/edstein)
2. python
   - Python 
     - [bot](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/bot) - лабораторная работа по Python - реализация телеграм-бота с парсингом статей с сайтов и последующей адаптацией для просмотра в приложении telegram 
     - [rickandmorty](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/rickandmorty) - лабораторная работа по Python - скрип по работе с модулем Requests и взаимодействию с api Rick and Morty и выводу результатов запросов в файл
     - [selenium](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/selenium) - лабораторная работа по Python - парсинг выдачи результатов поиска в steam с использованием selenium
     - [sirius_parsing](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/sirius_parsing) - лабораторная работа по Python - парсинг сайта университета Сириус
     - [vk](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/vk) - лабораторная работа по Python - парсинг среднего возраста друзей из VK
     - [yandexparsing](https://gitlab.com/TaffetaEarth/my_repo/-/tree/python/Python/yandexparsing) - парсинг чартов Яндекс Музыки
3. js
   - [express_db](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/js/express_db) - реализация вебапа на JS Express с использованием распределенной репликации Postgres, кэширования в Redis, поиска с автозаполнением через ElasticSearch, сервиса миграций бд FlyWay, подключенным ClickHouse
   - [socket_project](https://gitlab.com/TaffetaEarth/my_repo/-/tree/main/js/socket_project) - проект на TS с динамически обновляющемся фронтом приложения, взаимодествием с бд Postgres на сокетах  
