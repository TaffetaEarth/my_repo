package regularExpression;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RegularTest {

    public boolean flag = true;

    @BeforeEach
    void set() {
        flag = true;
    }

    @Test
    void russianPhone() {
        assertTrue(Regular.phoneValidate("+79182064673"));
    }

    @Test
    void russianPhoneWithDashes() {
        assertTrue(Regular.phoneValidate("+7-918-206-46-73"));
    }

    @Test
    void russianPhoneWithBrackets() {
        assertTrue(Regular.phoneValidate("+7(918)206-46-73"));
    }

    @Test
    void somePhone() {
        assertTrue(Regular.phoneValidate("+37522064673"));
    }

    @Test
    void somePhoneWithDashes() {
        assertTrue(Regular.phoneValidate("+375-2-206-46-73"));
    }

    @Test
    void somePhoneWithBrackets() {
        assertTrue(Regular.phoneValidate("+375(2)206-46-73"));
    }

    @Test
    void afterFiveWord() {
        String[] results = Regular.afterFive("В пятом часу, пятого дня, пятую ночь, пятым числом");
        String[] expected = new String[]{"часу", "дня", "ночь", "числом"};
        if (results.length == expected.length) {
            for (int i = 0; i < results.length; i++) {
                if (!results[i].equals(expected[i])) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }
        assertTrue(flag);
    }

    @Test
    void afterFiveWordFromStringStart() {
        String[] results = Regular.afterFive("распятом часу, пятого дня, пятую ночь, пятым числом");
        String[] expected = new String[]{"дня", "ночь", "числом"};
        if (results.length == expected.length) {
            for (int i = 0; i < results.length; i++) {
                if (!results[i].equals(expected[i])) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }
        assertTrue(flag);
    }

    @Test
    void afterSixWord() {
        String[] results = Regular.afterFive("Шестыми часами, шестого дня, шестую ночь, шестым числом");
        String[] expected = new String[0];
        if (results.length == expected.length) {
            for (int i = 0; i < results.length; i++) {
                if (!results[i].equals(expected[i])) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }
        assertTrue(flag);
    }

    @Test
    void wordReplacement() {
        assertEquals(Regular.textChange("Съешь еще немного этих мягких французских булок, да выпей чаю", new String[]{"мягких"}, "свежих"),
                "Съешь еще немного этих свежих французских булок, да выпей чаю");
    }

    @Test
    void manyWordReplacement() {
        assertEquals(Regular.textChange("Съешь еще немного этих мягких французских булок, да выпей чаю", new String[]{"мягких", "чаю"}, "свежих"),
                "Съешь еще немного этих свежих французских булок, да выпей свежих");
    }

    @Test
    void wordNotFoundReplacement() {
        assertEquals(Regular.textChange("Съешь еще немного этих мягких французских булок, да выпей чаю", new String[]{"свежих"}, "мягких"),
                "Съешь еще немного этих мягких французских булок, да выпей чаю");
    }
}