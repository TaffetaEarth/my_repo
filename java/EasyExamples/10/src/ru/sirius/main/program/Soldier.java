package ru.sirius.main.program;
import java.util.Random;

abstract class Soldier implements Move{
  private String name;
  Soldier(){
    this.name = "Nameless soldier";
  }
  abstract void move(){
  };
}
