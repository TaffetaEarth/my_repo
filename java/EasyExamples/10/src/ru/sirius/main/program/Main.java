package ru.sirius.main.program;
import java.util.Random;

class Main{
  public static void main(String[] args) {
    int n = new Random().nextInt(20);
    Soldier arr[] = new Soldier[n];
    int divernum = 0;
    int pilotnum = 0;
    for (int i = 0; i < n; i++) {
      int counter = new Random().nextInt(1);
      if (counter == 0) {
        arr[i] = new Diver();
      } else {
        arr[i] = new Pilot();
      }
      arr[i].move();
      if (arr[i] instanceof Diver) {
        divernum ++;
      } else {
        pilotnum ++;
      }
    }
    System.out.println("Общее пройденное расстояние: " + (Diver.sdiver + Pilot.spilot));
    System.out.println("Среднее пройденное расстояние подводниками: " + (double)Diver.sdiver/divernum);
  }
}
