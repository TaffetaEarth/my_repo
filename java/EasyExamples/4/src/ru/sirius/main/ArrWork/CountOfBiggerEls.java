package ru.sirius.main.ArrWork;


class CountOfBiggerEls{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 9, 8};
    int min = arr[0];
    for (int i = 0;i < arr.length; i++) {
      if (arr[i] < min) {
        min = arr[i];
      }
    }
    int counter = 0;
    for (int i = 1;i < arr.length; i++) {
      if (arr[i] == min + 5) {
        counter++;
      }
    }
    System.out.format("Количество элементов, больших минимального на 5: %d ", counter);
  }
}
