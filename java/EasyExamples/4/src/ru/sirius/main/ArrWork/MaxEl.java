package ru.sirius.main.ArrWork;


class MaxEl{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 9, 8};
    int counter = arr[0];
    for (int i = 1;i < arr.length; i++) {
      if (arr[i] > counter) {
        counter = arr[i];
      }
    }
    System.out.println(counter);
  }
}
