package ru.sirius.main.program;


class ArrayOfCustomClass{
  public static void main(String[] args) {
    int n = 5;
    CustomClass[] arr = new CustomClass[n];
    for (int i = 0;i < arr.length; i++) {
      arr[i] = new CustomClass("New name", 1, i);
    }
    arr[arr.length - 1].set_age(3);
    arr[arr.length - 1].set_name("George");
    System.out.format("%s %d %n", arr[arr.length - 1].name,
          arr[arr.length - 1].age);
  }
}
