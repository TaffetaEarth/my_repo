package ru.sirius.main.program;


class CustomClass{
  public String name;
  public int age;
  private static int id;

  CustomClass(String name, int age, int id){
    this.name = name;
    this.age = age;
    this.id = id;
  }

  public String get_name(){
    return this.name;
  }
  public int get_age(){
    return this.age;
  }
  public void set_name(String name){
    this.name = name;
  }
  public void set_age(int age){
    this.age = age;
  }
}
