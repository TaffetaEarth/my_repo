package ru.sirius.main.program;


class MakeObjects{
  public static void main(String[] args) {
    CustomClass n = new CustomClass("First", 1, 0);
    CustomClass m = new CustomClass("Second", 3, 1);
    System.out.format("%d %n", n.get_age());
    n.set_age(3);
    System.out.format("%d %n", n.get_age());
    System.out.format("%s %n", n.get_name());
    m.set_name("My name");
    System.out.format("%s %n", m.get_name());
    }
}
