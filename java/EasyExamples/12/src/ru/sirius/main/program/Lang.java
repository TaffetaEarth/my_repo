package ru.sirius.main.program;
import java.util.Random;


class MyNumber extends java.lang.Number{
  public double a;
  MyNumber(double a){
    this.a = a;
  }
  MyNumber(){
    this.a = new Random().nextInt();
  }
  public double doubleValue(){
    return (double) this.a;
  }
  public float floatValue(){
    return (float) this.a;
  }
  public int intValue(){
    return (int) this.a;
  }
  public long longValue(){
    return (long) this.a;
  }
}
