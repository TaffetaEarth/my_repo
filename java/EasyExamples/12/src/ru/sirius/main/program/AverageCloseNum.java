package ru.sirius.main.program;


public class AverageCloseNum {
  public static void main(String args[]){
    int[] array = new int[10];
    int sum = 0;
    for (int i = 0; i < array.length; i++){
      array[i] = (int)(Math.random()*10);
      System.out.format("%d ", array[i]);
      sum = sum + array[i];
    }

    double sr = (double)sum/array.length;
    double min = Math.abs(sr - (double)array[0]);
    int ele = array[0];
    for (int i = 0; i < array.length; i++){
      if ((Math.abs(sr - (double)array[i])) < min){
        min = Math.abs(sr - (double)array[i]);
        ele = array[i];
      }
    }
  }
}
