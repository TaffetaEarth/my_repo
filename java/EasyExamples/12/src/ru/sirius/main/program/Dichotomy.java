package ru.sirius.main.program;


class DichotomyMin{
  public static void main(String[] args) {
    double accuracy = 0.01;
    double start = -4;
    double end = 10;
    while (end - start >= accuracy){
      if (Func(start) < Func(end)){
        end -= (Math.abs(end) + Math.abs(start))/2;
      } else{
        start += (Math.abs(end) + Math.abs(start))/2;
      }
    }
    System.out.println("" + (start + (Math.abs(end) - Math.abs(start))/2) + " with accuracy " + accuracy/2);
  }
  public static double Func(double x){
    double y = Math.pow(x, 2);
    return y;
  }
}
