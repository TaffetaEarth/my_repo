package ru.sirius.main.program;
import java.util.Random;

class MoneyStorage{
  public int capacity;
  public int contain;
  MoneyStorage(){
    this.capacity = new Random().nextInt();
    this.contain = new Random().nextInt(this.capacity);
  }
  MoneyStorage(int capacity){
    this.capacity = capacity;
    this.contain = new Random().nextInt(this.capacity);
  }
  MoneyStorage(int capacity, int contain){
    this.capacity = capacity;
    this.contain = contain;
  }
}
