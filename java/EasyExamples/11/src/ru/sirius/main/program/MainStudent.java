package ru.sirius.main.program;
import java.util.Random;


class MainStudent{
  public static void main(String[] args) {
    RunningStudent st1 = new RunningStudent();
    st1.setName("Иванов Иван Иванович");
    st1.setTime(new Random().nextInt(60));
    System.out.println(st1.getInfo());
  }
}
