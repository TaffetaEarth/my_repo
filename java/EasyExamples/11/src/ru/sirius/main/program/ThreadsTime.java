package ru.sirius.main.program;
import java.util.Scanner;
import java.util.Date;


class ThreadsTime{
  public static void main(String[] args) {
    int n = 10;
    long time1 = new Date().getTime();
    for (int i = 0; i < n; i++) {
      try{
        Thread thread = new Thread(new ThreadsForTime());
        thread.start();
        thread.join();
      } catch (Exception e) {}
    }
    long time2 = new Date().getTime();
    System.out.println((time2 - time1)/1000);
  }
}
