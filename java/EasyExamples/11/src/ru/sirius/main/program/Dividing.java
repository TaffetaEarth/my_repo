package ru.sirius.main.program;


class Dividing{
  public static void main(String[] args) {
    double num = 10;
    int divider = 2;
    int counter = 0;
    while (num >= divider) {
      num = num/divider;
      counter++;
    }
    System.out.println(counter);
  }
}
