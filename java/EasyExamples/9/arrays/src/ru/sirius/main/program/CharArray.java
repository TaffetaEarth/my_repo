package ru.sirius.main.program;
import java.util.Random;


class CharArray{
  public static void main(String[] args) {
    int n = 10;
    String arr[] = new String[n];
    for (int i = 0; i < n; i++) {
      arr[i] = "";
      int len = new Random().nextInt(20);
      for (int j = 0; j < len; j++) {
        arr[i] += (char) (48 + new Random().nextInt(74));
      }
    }
    int isInt = 0;
    for (int i = 0; i < n; i++) {
      int counter = 0;
      for (int j = 1; j < arr[i].length(); j++) {
        if(48 <= (int)(arr[i].charAt(j))){
          if ((int)(arr[i].charAt(j)) <= 57){
            counter++;
          }
        }
      }
      if(counter == arr[i].length() - 1){
        isInt++;
      }
    }
    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i]);
    }
    System.out.println(isInt);
  }
}
