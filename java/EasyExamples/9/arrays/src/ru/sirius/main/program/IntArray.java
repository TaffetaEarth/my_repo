package ru.sirius.main.program;
import java.util.Random;


class IntArray{
  public static void main(String[] args) {
    int n = 10;
    String StringArr[] = new String[n];
    int intArr[] = new int[n];
    for (int i = 0; i < n; i++) {
      StringArr[i] = "";
      int len = new Random().nextInt(20);
      for (int j = 0; j < len; j++) {
        StringArr[i] += (char) (48 + new Random().nextInt(9));
      }
    }
    for (int i = 0; i < n; i++) {
      int counter = 1;
      int num = 0;
      for (int j = StringArr[i].length() - 1; j > 0; j--) {
        num += StringArr[i].charAt(j) * counter;
        counter = counter * 10;
          }
      intArr[i] = num;
      }
    for (int i = 0; i < intArr.length; i++) {
      System.out.println(intArr[i]);
      }
    int sum = 0;
    for (int i = 0; i < n; i++) {
      sum += intArr[i];
      }
    System.out.println("Среднее арифметическое массива: " + (double)sum/n);
    int lensum = 0;
    for (int i = 0; i < n; i++) {
      lensum += ("" + intArr[i]).length();
      }
    System.out.println("Средняя длина числа: " + (double)lensum/n);
  }
}
