package ru.sirius.main.program;
import java.util.Random;


class Main{
  public static void main(String[] args) {
    int n = 10;
    Hero arr[] = new Hero[n];
    for (int i = 0; i < n; i++) {
      int counter = new Random().nextInt(2);
      if (counter == 0) {
        arr[i] = new Warior("Warrior, уровень " + i, 100);
      }
      if (counter == 1) {
        arr[i] = new Wizard("Wizard, ранг " + i, 50);
      }
      if (counter == 2) {
        arr[i] = new Bird("Bird " + i, 10);
      }
    }
    for (int i = 0; i < n; i++) {
      arr[i].move();
      arr[i].getInfo();
      if (arr[i] instanceof Wizard) {
        ((Wizard) arr[i]).fly();
        System.out.println("I can Fly!");
      }
      if (arr[i] instanceof Bird) {
        ((Bird) arr[i]).fly();
        System.out.println("I can Fly!");
      }
      arr[i].getPlace();
    }
  }
}
