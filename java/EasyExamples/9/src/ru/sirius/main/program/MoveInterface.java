package ru.sirius.main.program;

interface Movable {
  public void move();
}
