package ru.sirius.main.program;
import java.util.Random;


class Bird extends Hero implements Fly{
  private String name;
  private int z;
  Bird(){
    this.name = "Nameless Bird";
  }
  Bird(String name, int health){
    super(name, health);
  }
  public void fly(){
    this.z = this.z + new Random().nextInt(10);
  }
  public void getPlace(){
    System.out.format("координаты: %d:%d:%d%n", this.x, this.y, this.z);
  }
}
