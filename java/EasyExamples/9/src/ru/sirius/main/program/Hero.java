package ru.sirius.main.program;
import java.util.Random;

abstract class Hero implements Movable{
  public int health;
  public int x;
  public int y;
  private String name;
  Hero(){
    this.name = "Nameless Hero";
    this.health = new Random().nextInt(1000);
    this.x = 0;
    this.y = 0;
  }
  Hero(String name, int health){
    this.name = name;
    this.health = health;
    this.x = 0;
    this.y = 0;
  }
  // public void fly(){}
  public void move(){
    this.x += new Random().nextInt(10);
    this.y += new Random().nextInt(10);
  }
  public void getPlace(){
    System.out.format("координаты: %d:%d%n", this.x, this.y);
  }
  public void getInfo(){
    System.out.format("Имя: %s, Здоровье: %d%n", this.name, this.health);
  }
}
