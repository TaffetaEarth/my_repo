package ru.sirius.main.program;
import java.util.Random;

class Wizard extends Hero implements Fly{
  private String name;
  private int z;
  Wizard(){
    this.name = "Nameless Wizard";
  }
  Wizard(String name, int health){
    super(name, health);
  }
  public void fly(){
    this.z += new Random().nextInt(10);
    }
  public void getPlace(){
    System.out.format("координаты: %d:%d:%d%n", this.x, this.y, this.z);
  }
}
