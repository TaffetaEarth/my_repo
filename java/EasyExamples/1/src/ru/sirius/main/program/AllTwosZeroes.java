package ru.sirius.main.program;

class AllTwoZeroes{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3};
    int last = arr.length;
    int i  = 1;
    while(i < last){
      if(arr[i] % 2 == 0){
        arr[i] = 0;
        i++;
      }
    }
    for(int k = 0; k <= last; k++){
      System.out.format("%d ", arr[k]);
    }
  }
}
