package ru.sirius.main.program;

class AllTwoReplace{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    int last = arr.length;
    int i  = 0;
    int k = 1;
    while(i < last){
      if(arr[i] % 2 == 0){
        if(k % 2 == 0){
          arr[i] = arr[last-1];
          k++;
        } else {
          k++;
        }
      }
      i++;
    }
    for(int t = 0; t < last; t++){
      System.out.format("%d ", arr[t]);
    }
  }
}
