package ru.sirius.main.program;

class ReplaceFirstAndLast{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3};
    int n = arr[0];
    int last = arr.length;
    arr[0] = arr[last];
    arr[last] = n;
    for(int i = 0; i <= last; i++){
      System.out.format("%d ", arr[i]);
    }
  }
}
