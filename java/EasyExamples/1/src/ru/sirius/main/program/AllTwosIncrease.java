package ru.sirius.main.program;

class AllTwoIncrease{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3};
    int last = arr.length;
    int i  = 1;
    while(i < last){
      arr[i] = arr[i]*2;
      i = i + 2;
    }
    for(int k = 0; k <= last; k++){
      System.out.format("%d ", arr[k]);
    }
  }
}
