package ru.sirius.main.program;


class ArrClass{
  public int[] arr;
  ArrClass(int n){
    arr = new int[n];
    for (int i = 0;i < n; i++) {
      this.arr[i] = (int) (Math.random() * 10);
    }
  }
  public double Aver(){
    double k = 0;
    for (int i = 0;i < this.arr.length; i++) {
      k = k + (double) this.arr[i];
    }
    return k / this.arr.length;
  }
}
