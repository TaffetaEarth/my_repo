package ru.sirius.main.program;


class Dispersion{
  public static void main(String[] args) {
    int n = (int) (Math.random() * 10);
    int[][] arr = new int[n][];
    for (int i = 0; i < arr.length; i++) {
      int m = (int) (Math.random() * 10);
      arr[i] = new int[m];
    }
    int counter = 0;
    for (int i = 0; i < arr.length; i++) {
      counter = counter + arr[i].length;
    }
    double average = counter/arr.length;
    double dis_counter = 0;
    for (int i = 0; i < arr.length; i++) {
      dis_counter = dis_counter + Math.pow(arr[i].length - average, 2);
    }
    System.out.println(dis_counter/arr.length);
  }
}
