package ru.sirius.main.program;


class TenClassElements{
  public static void main(String[] args) {
    int len = 10;
    CustomClass[] arr = new CustomClass[len];
    for (int n = 0; n < len; n++) {
      arr[n] = new CustomClass();
    }
    for (int i = 0;i < len; i++) {
      if (arr[i].FieldInt > 100) {
        System.out.println(arr[i]);
      }
    }
  }
}
