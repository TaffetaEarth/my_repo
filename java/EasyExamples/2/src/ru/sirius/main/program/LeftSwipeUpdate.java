package ru.sirius.main.program;

class LeftSwipeUpdate{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8};
    int swipe = 2;
    int[] new_arr = new int[arr.length];
    for(int k = 0; k < swipe; k++){
      new_arr[k] = 0;
    }
    for(int i = 0; i < new_arr.length - swipe; i++){
      new_arr[i+swipe] = arr[i];
    }
    for(int j = 0; j < new_arr.length; j++){
      System.out.format("%d ", new_arr[j]);
    }
  }
}
