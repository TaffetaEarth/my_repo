package ru.sirius.main.program;


class ChangeSign{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9,
       10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    for(int i = 0; i < (arr.length - 1); i++){
      if(arr[i] > 10){
        arr[i] = arr[i] * (-1);
      }
    }
    for(int k = 0; k < (arr.length - 1); k++){
      System.out.format("%d ", arr[k]);
    }
  }
}
