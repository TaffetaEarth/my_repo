package ru.sirius.main.program;


class PlusTwos{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8};
    for(int i = 0; i < arr.length; i++){
      if(arr[i] > 0 & arr[i] % 2 == 0){
          System.out.format("%d ", arr[i]);
      }
    }
  }
}
