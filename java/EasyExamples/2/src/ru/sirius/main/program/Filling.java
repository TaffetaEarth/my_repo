package ru.sirius.main.program;


class Filling{
  public static void main(String[] args) {
    int n = 5;
    int[] arr = new int[n];
    for(int i = 0; i < arr.length; i++){
      arr[i] = (int) Math.pow(i, 2) + 10;
    }
    for(int k = 0; k < arr.length; k++){
      System.out.format("%d ", arr[k]);
    }
  }
}
