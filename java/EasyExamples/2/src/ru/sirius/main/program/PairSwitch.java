package ru.sirius.main.program;

class PairSwitch{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int i = 0;
    int n = 0;
    while(i < arr.length - 1){
      n = arr[i];
      arr[i] = arr[i+1];
      arr[i+1] = n;
      i = i + 2;
    }
    for(int k = 0; k < arr.length; k++){
      System.out.format("%d ", arr[k]);
    }
  }
}
