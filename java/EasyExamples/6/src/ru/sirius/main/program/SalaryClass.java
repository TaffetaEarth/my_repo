package ru.sirius.main.program;


class SalaryClass{
  private int white;
  private int black;
  SalaryClass(int white, int black){
    this.white = white;
    this.black = black;
  }
  public int getSalary(){
    return this.white;
  }
  public void setSalary(int a){
    this.black = a;
  }
  public String toString(){
    return Integer.toString(this.black + this.white);
  }
}
