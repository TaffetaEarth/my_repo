package ru.sirius.main.program;


class MainSalary{
  public static void main(String[] args) {
    SalaryClass pers = new SalaryClass(10000, 25000);
    System.out.format("%d%n%s%n", pers.getSalary(), pers.toString());
    pers.setSalary(20000);
    System.out.format("%s%n", pers.toString());
  }
}
