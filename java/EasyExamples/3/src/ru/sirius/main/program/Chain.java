package ru.sirius.main.program;

class Chain{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 1, 8, 9,
      10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    int k = 1;
    int kmax = 0;
    for(int i = 0; i < arr.length - 1; i++){
      if(arr[i] == arr[i+1] - 1){
        k++;
      }else{
        if(k > kmax){
          kmax = k;
        }
        k = 1;
      }
    }
    if(k > kmax){
      kmax = k;
    }
    if(kmax == 1){
      kmax = 0;
    }
    System.out.format("%d ", kmax);
  }
}
