package ru.sirius.main.program;


class Dispersion{
  public static void main(String[] args) {
    double [] arr = new double[] {1, 2, 3, 4, 5, 0, 7, 8, 9,
      10, 11, 12, 13, 14, 15, 1, 17, 18, 19};
    double k = 0;
    for(int i = 0; i < arr.length; i++){
      k = k + arr[i];
    }
    double av = (double) k / arr.length;
    for(int i = 0; i < arr.length; i++){
      if(arr[i] > 0){
        System.out.format("Для элемента %d дисперсия составит %f%n", i, arr[i] - av);
      }
    }
  }
}
