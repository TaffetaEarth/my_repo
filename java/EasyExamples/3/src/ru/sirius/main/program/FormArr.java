package ru.sirius.main.program;

class FormArr{
  public static void main(String[] args) {
    int n = 12;
    double k = 5;
    double[] arr = new double[n];
    boolean o = true;
    for(int i = 0; i < n; i++){
      arr[i] = i*i/k;
      if(arr[i] > i){
        System.out.println(i);
        o = false;
        break;
      }
    }
    if(o == true){
      System.out.println("Нет значений");
    }
  }
}
