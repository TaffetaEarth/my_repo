package ru.sirius.main.program;

class Average{
  public static void main(String[] args) {
    int[] arr = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9,
      10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    double counter = 0;
    for(int i = 0; i < arr.length; i++){
      counter = counter + arr[i];
    }
    System.out.format("%f ", counter/arr.length);
  }
}
