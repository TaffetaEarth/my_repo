package ru.sirius.main.program;


class Main{
  public static void main(String[] args) {
    // test1();
    // test2();
    test3();
  }
  public static void test1(){
    Student st1 = new Student();
    Student st2 = new Student("Mike Vazovski", "Monsters");
    System.out.println(st2.getname());
    st2.setgroup("New Monsters");
    System.out.println(st2.getgroup());
    System.out.println();
  }




  public static void test2(){
    BestStudent bst1 = new BestStudent("Only-5-Student",
                                              "Best Students");
    BestStudent bst2b= new BestStudent();
    System.out.println(bst1.getname());
    System.out.println(bst1.getgroup());
    bst1.sayhooray();
    System.out.println(Student.studentsquan);
  }




  public static void test3(){
    BestStudent[] arr = new BestStudent[5];
    for (int i = 0; i < arr.length; i++) {
      String name = "bs" + (i + 1);
      arr[i] = new BestStudent(name, "Best Students");
    }
    for (int i = 0; i < arr.length; i++) {
      System.out.print(arr[i].getname() + " ");
      System.out.print(arr[i].getgroup() + " ");
      arr[i].sayhooray();
    }
    System.out.println(Student.studentsquan);
  }




}
