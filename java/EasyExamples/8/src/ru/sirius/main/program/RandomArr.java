package ru.sirius.main.program;
import java.util.Random;


class RandomArr{
  public static void main(String[] args) {
    Random random = new Random();
    Student[] arr =
            new Student[random.nextInt(20)];
    for (int i = 0; i < arr.length; i++) {
      if (random.nextBoolean() == true){
        arr[i] = new Student();
      } else {
        arr[i] = new BestStudent();
      }
    }
    int counter = 0;
    for (int i = 0; i < arr.length; i++) {
        if (arr[i].isBestStudent()){
          counter++;
        }
      }
      System.out.println(counter);
      double percent;
      if (Student.studentsquan == 0){
        percent = 100;
      } else {
        percent = ((double) BestStudent.beststudnum/Student.studentsquan) * 100;
      }
      System.out.println("Процент студентов-отличников: "
          + percent + "%");
      System.out.println(BestStudent.beststudnum);
      System.out.println(Student.studentsquan);
      BestStudent jack = new BestStudent("Jack");
      System.out.println(jack.getName());
      System.out.println(jack.getGroup());
      System.out.println(new CompareStudents().compare(new BestStudent("Jack"), new Student("James")));
      // jack.sayhooray();
  }
}
