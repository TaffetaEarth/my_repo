package serialization;

import java.io.Serializable;

public class SerializableObject implements Serializable {

    public static final long serialVersionUID = 1L;

    public Integer id;

    public String name;

    SerializableObject(){}

    SerializableObject(Integer id, String name){
        this.id = id;
        this.name = name;
    }
}
