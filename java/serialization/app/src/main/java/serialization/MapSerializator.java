package serialization;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * класс для сериализации отображения
 */

public class MapSerializator {
    /**
     * размер отображения
     */
    public static final Integer KEY_AMOUNT = 100;
    /**
     * путь к файлу для сохранения
     */
    public static final String FILEPATH = "./src/main/resources/bufferfile.txt";

    /**
     * метод генерации изображения
     * @return сгенерированное отображение
     */
    public static Map<Integer, SerializableObject> generateMap(){
        Map<Integer, SerializableObject> map = new HashMap<>();
        for (int i = 0; i < KEY_AMOUNT; i++) {
            String value = "";
            for (int j = 0; j < 10; j++) {
                value += (char) new Random().nextInt(127);
            }
            map.put(new Random().nextInt(Integer.MAX_VALUE), new SerializableObject(i, value));
        }
        return map;
    }

    /**
     * метод для сериализации отображения в файл
     * @param filepath - путь к файлу для сериализации
     * @param map - отображение, которое нужно сериализовать
     */
    public static void saveMap(String filepath, Map map) {
        try (
                FileOutputStream fileOutputStream = new FileOutputStream(filepath);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)
        ) {
            objectOutputStream.writeObject(map);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * метод для чтения сериализованного отображения из файла
     * @param filepath - путь к файлу с сериализованным отображением
     * @return - отображение, прочитанное из файла
     */
    public static Map<Integer, SerializableObject> readMap(String filepath) {
        try (
                FileInputStream fileInputStream = new FileInputStream(filepath);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)
        ) {
            return (Map<Integer, SerializableObject>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args){
        String filepath = args.length != 0 ? args[0] : FILEPATH;
        saveMap(filepath, generateMap());
        System.out.println(readMap(filepath).toString());
    }
}
