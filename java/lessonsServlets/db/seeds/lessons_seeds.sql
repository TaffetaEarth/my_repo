insert into lessons (timeslot_id, group_id, audience_id, teacher_id, subject_id)
select (select id from timeslots where time_start = '9:00'),
       (select id from groups where group_name = '9.7.1'),
       (select id from audiences where cabinet = '307'),
       (select id from teachers where name = 'Иван В.'),
       (select id from subjects where name = 'БД');