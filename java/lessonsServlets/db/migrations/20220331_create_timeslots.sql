create table timeslots
(
    id         int generated always as identity not null primary key,
    time_start time                             not null,
    time_end   time                             not null
);
