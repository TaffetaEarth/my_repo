create table serialized_lessons
(
    id        int generated always as identity primary key,
    lesson_id int references lessons,
    object    bytea not null
)
