create table teachers
(
    id   int generated always as identity not null primary key,
    name text                             not null
);
