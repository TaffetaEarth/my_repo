create table groups
(
    id         int generated always as identity not null primary key,
    group_name text                             not null
);
