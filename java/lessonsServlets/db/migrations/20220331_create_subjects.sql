create table subjects
(
    id   int generated always as identity not null primary key,
    name text                             not null
);

