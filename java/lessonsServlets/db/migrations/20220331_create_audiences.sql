create table audiences
(
    id      int generated always as identity not null primary key,
    cabinet text                             not null
);