create table lessons
(
    id          int generated always as identity primary key,
    timeslot_id int references timeslots,
    group_id    int references groups,
    audience_id int references audiences,
    teacher_id  int references teachers,
    subject_id  int references subjects
);