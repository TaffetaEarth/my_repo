package Connect;

import java.sql.*;
import java.util.Properties;

/**
 * Class to connect app with db
 *
 * @author taffetaearth
 */
public class Connect {
    /**
     * field for single
     */
    private static Connection conn;

    public static Connection getConnection() {
        String dbpath = "jdbc:postgresql://127.0.0.1:5000/postgres";
        if (conn == null) {
            conn = createConnection(dbpath);
            return conn;
        } else {
            System.out.println("Connection already exist!");
            return null;
        }
    }

    public static Connection createConnection(String dbpath) {
        try {
            Properties connectionProps = new Properties();
            connectionProps.put("user", "taffetaearth");
            connectionProps.put("password", "123");
            return DriverManager.getConnection(dbpath, connectionProps);
        } catch (Exception e) {
            System.out.println("Connection failed!");
            return null;
        }
    }

    public static ResultSet execute(String query) throws SQLException {
        Statement stmt;
        try {
            stmt = conn.createStatement();
            return stmt.executeQuery(query);
        } catch (SQLException | NullPointerException e) {
            System.out.println("Execution failed!\n" + e);
            return null;
        }
    }

    public static void executeWithoutReturn(String query) {
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException | NullPointerException e) {
            System.out.println("Execution failed!\n" + e);
        }
    }


}
