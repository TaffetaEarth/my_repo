package Controllers;

import DTO.LessonWrapper;
import Models.Lesson;
import Services.LessonSerializer;
import QueryObject.*;
import ParametersGetter.*;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;

import static Services.IdChecker.idNotExists;

@WebServlet("/serialized_lessons/*")
public class SingleSerializeController extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String idParameter = ParametersGetter.getId(request);
        int id = Integer.parseInt(idParameter);
        try {
            if (!idNotExists(id, Lesson.SERIALIZED_TABLE)) {
                Lesson deserializedLesson = LessonSerializer.getDeserialized(id);
                request.setAttribute("lesson", QueryObject.wrap(deserializedLesson));
                getServletContext().getRequestDispatcher("/WEB-INF/classes/Views/show.jsp").forward(request, response);
            } else {
                if (!idNotExists(id, Lesson.MAIN_TABLE)) {
                    Lesson lesson = Lesson.getById(id, (rs) ->
                            new Lesson(rs.getInt("id"),
                                    rs.getInt("timeslot_id"),
                                    rs.getInt("audience_id"),
                                    rs.getInt("group_id"),
                                    rs.getInt("teacher_id"),
                                    rs.getInt("subject_id")));
                    request.setAttribute("lesson", QueryObject.wrap(lesson));
                    getServletContext().getRequestDispatcher("/WEB-INF/classes/Views/show.jsp").forward(request, response);
                } else {
                    response.sendError(404);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        String idParameter = ParametersGetter.getId(request);
        if (idParameter != null) {
            int id = Integer.parseInt(idParameter);
            try {
                if (!idNotExists(id, Lesson.MAIN_TABLE)) {
                    Lesson lesson = Lesson.getById(id, (rs) ->
                            new Lesson(rs.getInt("id"),
                                    rs.getInt("timeslot_id"),
                                    rs.getInt("audience_id"),
                                    rs.getInt("group_id"),
                                    rs.getInt("teacher_id"),
                                    rs.getInt("subject_id")));
                    LessonSerializer.createSerialized(lesson);
                } else {
                    response.sendError(404);
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        response.sendRedirect("/lessons/serialized_lessons");
    }
}