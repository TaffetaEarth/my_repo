package Controllers;

import javax.servlet.http.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.annotation.WebServlet;

import Connect.Connect;
import QueryObject.QueryObject;
import Models.*;
import ParametersGetter.*;

/**
 * @author TaffetaEarth
 * SingleEntity class of Lessons Servlet
 * @version 1.0
 */
@WebServlet("/lessons/*")
public class SingleLessonController extends HttpServlet {

    /**
     * method to handle PUT http request
     *
     * @param request  - request
     * @param response - response to create answer
     * @throws IOException - may be thrown IOException
     */
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        request.setCharacterEncoding("UTF-8");
        int id = Integer.parseInt(ParametersGetter.getId(request));
        try {
            if (idNotExists(id)) {
                response.sendError(404, "No such id");
            } else {
                String body = ParametersGetter.getBody(request);
                String timeslot = ParametersGetter.getTimeSlotId(body);
                String group = ParametersGetter.getGroupId(body);
                String audience = ParametersGetter.getAudienceId(body);
                String teacher = ParametersGetter.getTeacherId(body);
                String subject = ParametersGetter.getSubjectId(body);
                if (timeslot == null || group == null || audience == null || teacher == null || subject == null) {
                    response.sendError(422, "Wrong parameters");
                } else {
                    int timeslotId = Integer.parseInt(timeslot);
                    int groupId = Integer.parseInt(group);
                    int audienceId = Integer.parseInt(audience);
                    int teacherId = Integer.parseInt(teacher);
                    int subjectId = Integer.parseInt(subject);
                    Lesson lesson = new Lesson(id, timeslotId, groupId, audienceId, teacherId, subjectId);
                    lesson.update();
                    response.sendRedirect(request.getRequestURL().toString());
                }
            }
        } catch (SQLException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    /**
     * method to handle GET http request to a single entity
     *
     * @param request  - request
     * @param response - response to create answer
     * @throws IOException - may be thrown IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        PrintWriter out = setting(response);
        int id = Integer.parseInt(ParametersGetter.getId(request));
        try {
            if (idNotExists(id)) {
                response.sendError(404);
            } else {
                Lesson lesson = (Lesson) Lesson.getById(id, (rs) ->
                        new Lesson(rs.getInt("id"),
                        rs.getInt("timeslot_id"),
                        rs.getInt("audience_id"),
                        rs.getInt("group_id"),
                        rs.getInt("teacher_id"),
                        rs.getInt("subject_id")));
                out.println(QueryObject.wrap(lesson).toString());
            }
        } catch (SQLException e) {
            out.println("<p>" + e + "</p>");
        }

    }


    /**
     * method to handle DELETE http request
     *
     * @param request  - request
     * @param response - response to create answer
     * @throws IOException - may be thrown IOException
     */
    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        PrintWriter out = setting(response);
        int id = Integer.parseInt(ParametersGetter.getId(request));
        try {
            if (idNotExists(id)) {
                response.sendError(404);
            } else {
                Lesson lesson = new Lesson(id);
                lesson.delete();
                Pattern pattern = Pattern.compile("/\\d+");
                Matcher matcher = pattern.matcher(request.getRequestURI());
                String newRequest = "";
                matcher.find();
                newRequest = matcher.replaceAll("");
                response.sendRedirect(newRequest);
            }
        } catch (SQLException e) {
            out.println("<p>" + e + "</p>");
        }
    }

    /**
     * method to set up headers of response
     *
     * @param response - response to create answer
     * @throws IOException - may be thrown IOException
     */
    public PrintWriter setting(HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head><title>Расписание Студентов</title></head>");
        return out;
    }

    public static boolean idNotExists(int id) throws SQLException {
        String query = "select id from lessons where id = " + id;
        ResultSet rs = Connect.execute(query);
        return rs == null || !rs.next();
    }
}
