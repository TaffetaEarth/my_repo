package Controllers;

import QueryObject.QueryObject;
import Models.*;
import ParametersGetter.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;


@WebServlet("/lessons")
public class MultipleLessonsController extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        ArrayList<Lesson> lessons = (ArrayList) Lesson.getAll((rs) ->
                new Lesson(rs.getInt("id"),
                        rs.getInt("timeslot_id"),
                        rs.getInt("audience_id"),
                        rs.getInt("group_id"),
                        rs.getInt("teacher_id"),
                        rs.getInt("subject_id")));
        for (Lesson lesson : lessons) {
            out.println("<p>" + QueryObject.wrap(lesson).toString() + "</p>");
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        String body = ParametersGetter.getBody(request);
        String timeslot = ParametersGetter.getTimeSlotId(body);
        String group = ParametersGetter.getGroupId(body);
        String audience = ParametersGetter.getAudienceId(body);
        String teacher = ParametersGetter.getTeacherId(body);
        String subject = ParametersGetter.getSubjectId(body);
        if (timeslot == null || group == null || audience == null || teacher == null || subject == null) {
            response.sendError(422, "Wrong parameters");
        } else {
            int timeslotId = Integer.parseInt(timeslot);
            int groupId = Integer.parseInt(group);
            int audienceId = Integer.parseInt(audience);
            int teacherId = Integer.parseInt(teacher);
            int subjectId = Integer.parseInt(subject);
            Lesson newLesson = Lesson.getIdByParameters(subjectId, groupId, audienceId, timeslotId, teacherId, (rs) ->
                    new Lesson(rs.getInt("id")));
            if (newLesson == null) {
                newLesson = new Lesson(timeslotId, groupId, audienceId, teacherId, subjectId);
                newLesson.create();
                newLesson.id = Lesson.getIdByParameters(subjectId, groupId, audienceId, timeslotId, teacherId, (rs) ->
                        new Lesson(rs.getInt("id"))).getId();
            }
            int id = newLesson.getId();
            response.sendRedirect(request.getRequestURL().toString() + "/" + id);
        }
    }
}
