package Controllers;

import DTO.*;
import Models.*;
import QueryObject.QueryObject;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;

@WebServlet("/report")
public class ReportController extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        String group = request.getParameter("group");
        ArrayList<TimeTeacherGroupWrapper> wrappers = new ArrayList<>();
        if (group != null) {
            ArrayList<Lesson> lessons = Lesson.getByGroup(group, (rs) -> new Lesson(rs.getInt("id"),
                    rs.getInt("timeslot_id"),
                    rs.getInt("audience_id"),
                    rs.getInt("group_id"),
                    rs.getInt("teacher_id"),
                    rs.getInt("subject_id")));
            if (lessons != null) {
                for (Lesson lesson : lessons) {
                    wrappers.add(QueryObject.reportWrap(lesson));
                }
            }
        }
        request.setAttribute("wrappers", wrappers.toArray(new TimeTeacherGroupWrapper[0]));
        getServletContext().getRequestDispatcher("/WEB-INF/classes/Views/button.jsp").forward(request, response);
    }
}

