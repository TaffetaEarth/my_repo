package Controllers;

import DTO.LessonWrapper;
import Models.Lesson;
import Services.LessonSerializer;
import QueryObject.*;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.annotation.WebServlet;

import static Services.IdChecker.idNotExists;

@WebServlet("/serialized_lessons")
public class SerializeController extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=UTF-8");
        ArrayList<Lesson> lessons = (ArrayList) Lesson.getAll((rs) ->
                new Lesson(rs.getInt("id"),
                        rs.getInt("timeslot_id"),
                        rs.getInt("audience_id"),
                        rs.getInt("group_id"),
                        rs.getInt("teacher_id"),
                        rs.getInt("subject_id")));
        LessonWrapper[] wrappers = new LessonWrapper[lessons.size()];
        for (int i = 0; i < wrappers.length; i++) {
            wrappers[i] = QueryObject.wrap(lessons.get(i));
        }
        request.setAttribute("lessons", wrappers);
        getServletContext().getRequestDispatcher("/WEB-INF/classes/Views/deserialize.jsp").forward(request, response);
    }


}