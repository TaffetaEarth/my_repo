package DTO;

import java.sql.SQLException;

public class TimeTeacherGroupWrapper {
    public String timeslot;

    public String group;
    public String teacher;


    public String getTimeslot() {
        return timeslot;
    }

    public String getGroup() {
        return group;
    }

    public String getTeacher() {
        return teacher;
    }


    public TimeTeacherGroupWrapper(String timeslot, String group, String teacher) throws SQLException {
        this.timeslot = timeslot;
        this.group = group;
        this.teacher = teacher;
    }

    public String toString(){
        return this.getTimeslot() + "\t" + this.getGroup() + "\t" + this.getTeacher();
    }
}
