package DTO;

import java.sql.SQLException;

public class LessonWrapper implements Wrapper{

    public Integer id;

    public String timeslot;
    public String audience;
    public String group;
    public String teacher;
    public String subject;

    public Integer getId() {
        return id;
    }

    public String getTimeslot() {
        return timeslot;
    }

    public String getAudience() {
        return audience;
    }

    public String getGroup() {
        return group;
    }

    public String getTeacher() {
        return teacher;
    }

    public String getSubject() {
        return subject;
    }

    public LessonWrapper(Integer id, String timeslot, String audience, String group, String teacher, String subject) throws SQLException {
        this.id = id;
        this.timeslot = timeslot;
        this.audience = audience;
        this.group = group;
        this.teacher = teacher;
        this.subject = subject;
    }

   public String toString(){
        return this.getTimeslot() + "\t" + this.getGroup() + "\t" + this.getSubject() + "\t" + this.getAudience() + "\t" + this.getTeacher();
    }
}
