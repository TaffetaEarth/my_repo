package Models;


import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

import Connect.*;

public class Lesson extends BasicModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String MAIN_TABLE = "lessons";
    public static final String SERIALIZED_TABLE = "serialized_lessons";

    public Integer id;
    public int timeslotId;
    public int groupId;
    public int audienceId;
    public int teacherId;
    public int subjectId;

    public Integer getId() {
        return id;
    }

    public int getTimeslotId() {
        return timeslotId;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getAudienceId() {
        return audienceId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    private Lesson(){}

    public Lesson(int id, int timeslotId, int groupId, int audience, int teacher, int subject) {
        this.id = id;
        this.timeslotId = timeslotId;
        this.groupId = groupId;
        this.audienceId = audience;
        this.teacherId = teacher;
        this.subjectId = subject;
    }

    public Lesson(int timeslotId, int groupId, int audience, int teacher, int subject) {
        this.timeslotId = timeslotId;
        this.groupId = groupId;
        this.audienceId = audience;
        this.teacherId = teacher;
        this.subjectId = subject;
    }

    public Lesson (int id){
        super(id);
    }


    public boolean equals(Lesson lesson) {
        return  this.timeslotId != lesson.timeslotId &&
                this.groupId != lesson.groupId &&
                this.audienceId != lesson.audienceId &&
                this.teacherId != lesson.teacherId &&
                this.subjectId != lesson.subjectId;
    }

    public static ArrayList<Lesson> getAll(Expression ex) {
        return (ArrayList) BasicModel.getAll(ex, MAIN_TABLE);
    }

    public static Lesson getById(int id, Expression ex) {
        return (Lesson) BasicModel.getById(id, ex, MAIN_TABLE);
    }

    public static ArrayList<Lesson> getByGroup(String group, Expression ex) {
        ArrayList<Lesson> lessons = new ArrayList<Lesson>();
        try {
            ResultSet rs = execute("select * " +
                    "from " + MAIN_TABLE + " " +
                    "where lessons.group_id = " +
                    "(select id from groups where group_name = '" + group + "')");
            if (rs != null){
                getByParameters((ArrayList) lessons, rs, ex);
                return lessons;
            }
        } catch (SQLException e){
            return null;
        }
        return null;
    }


    public static Lesson getIdByParameters(int subject, int group, int audience, int timeSlot, int teacher, Expression ex) {
        ArrayList<Lesson> lessons = new ArrayList<Lesson>();
        try {
            ResultSet rs = execute("select id " +
                    "from " + MAIN_TABLE +
                    "where subject_id = " + subject +
                    " and group_id = " + group +
                    " and audience_id = " + audience +
                    " and timeslot_id = " + timeSlot +
                    " and teacher_id = " + teacher);
            if (rs != null){
                getByParameters((ArrayList) lessons, rs, ex);
                if (lessons.size() != 0){
                    return lessons.get(0);
                }
            }
        } catch (SQLException e){
            return null;
        }
        return null;
    }

    public void update() {
        executeWithoutReturn("update " + MAIN_TABLE + " set timeslot_id = " + this.timeslotId +
                ", group_id = " + this.groupId +
                ", audience_id = " + this.audienceId +
                ", teacher_id = " + this.teacherId +
                ", subject_id = " + this.subjectId +
                " where id = " + this.id);
    }

    public void create() {
        executeWithoutReturn("insert into " +  MAIN_TABLE + " (timeslot_id," +
                " group_id, audience_id, teacher_id, subject_id) values " +
                "(" +
                "'" + this.timeslotId + "', '" +
                this.groupId + "', '" +
                this.audienceId + "', '" +
                this.teacherId + "', '" +
                this.subjectId + "'" +
                ")");
    }

    public byte[] serialize() {
        try(
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            ObjectOutputStream outputObject = new ObjectOutputStream(output);
            )
        {
            outputObject.writeObject(this);
            System.out.println("written");
            outputObject.close();
            output.close();
            return output.toByteArray();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }


    public static Lesson deserialize(byte[] bytes) {
        try
            {
                ByteArrayInputStream input = new ByteArrayInputStream(bytes);
                ObjectInputStream inputObject = new ObjectInputStream(input);
                inputObject.close();
                input.close();
                return (Lesson) inputObject.readObject();
            } catch (Exception e){
                System.out.println(e.getMessage());
                return null;
            }
        }

    public void delete() {
        executeWithoutReturn("delete from " + MAIN_TABLE + " where id = " + this.id);
    }



}

