package Models;

import Connect.Connect;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BasicModel {

    public Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    protected BasicModel(){}

    public BasicModel(int id) {
        this.id = id;
    }

    public static ResultSet execute(String query) {
        Connect.getConnection();
        try {
            return Connect.execute(query);
        } catch (SQLException e) {
            return null;
        }
    }

    public static void executeWithoutReturn(String query) {
        Connect.getConnection();
        Connect.executeWithoutReturn(query);
    }

    public static void getByParameters(ArrayList<BasicModel> models, ResultSet rs, Expression ex) throws SQLException {
        while (rs.next()){
            models.add(ex.formQuery(rs));
        }
    }

    public static ArrayList<BasicModel> getAll(Expression ex, String tableName) {
        ArrayList<BasicModel> models = new ArrayList<BasicModel>();
        try {
            ResultSet rs = execute("select * from " + tableName);
            if (rs != null){
                getByParameters(models, rs, ex);
                return models;
            }
        } catch (SQLException e){
            return null;
        }
        return null;
    }

    public static BasicModel getById(int id, Expression ex, String tableName) {
        ArrayList<BasicModel> models = new ArrayList<BasicModel>();
        try {
            ResultSet rs = execute("select *" +
                    "from " + tableName +
                    " where id = " + id);
            getByParameters(models, rs, ex);
            return models.get(0);
        } catch (SQLException e) {
            System.out.println(e);;
        }
        return null;
    }

}
