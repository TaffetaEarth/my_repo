package Models;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Expression {
    BasicModel formQuery(ResultSet rs) throws SQLException;
}
