package Services;

import Connect.Connect;
import Models.Lesson;
import QueryObject.QueryObject;


import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;

import static Models.BasicModel.execute;


public class LessonSerializer {

    public static void createSerialized(Lesson lesson) throws SQLException {
        Connection connection = Connect.getConnection();
        PreparedStatement statement = connection.prepareStatement("insert into " + Lesson.SERIALIZED_TABLE + " (lesson_id, object) values " +
                "(" + lesson.getId() + ", ?)");
        statement.setBytes(1, lesson.serialize());
        statement.executeUpdate();
    }

    public static Lesson getDeserialized(int id) {
        try (ResultSet rs = execute("select *" +
                "from " + Lesson.SERIALIZED_TABLE +
                " where lesson_id = " + id +
                " order by id desc" +
                " limit 1")
        ) {
            if (rs != null) {
                rs.next();
                byte[] serializedObject = rs.getBytes("object");
                return Lesson.deserialize(serializedObject);
             }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
}
