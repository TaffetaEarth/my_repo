package QueryObject;

import DTO.LessonWrapper;
import DTO.TimeTeacherGroupWrapper;
import Models.Lesson;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class QueryObject {
    public static LessonWrapper wrap(Lesson lesson) {
        if (lesson.getId() == null) {
            return null;
        }
        try {
            String query = "select l.id as id, subjects.name as subject, group_name as group, cabinet as cab, time_start as beg, time_end as end, teachers.name as teacher " +
                    "from lessons l " +
                    "join subjects on l.subject_id = subjects.id " +
                    "join groups on l.group_id = groups.id " +
                    "join audiences on l.audience_id = audiences.id " +
                    "join timeslots on l.timeslot_id = timeslots.id " +
                    "join teachers on l.teacher_id = teachers.id " +
                    "where l.id = " + lesson.getId();
            ResultSet rs = Lesson.execute(query);
            if (rs != null) {
                rs.next();
            }
            return new LessonWrapper(rs.getInt("id"), rs.getString("beg") + " - " + rs.getString("end"), rs.getString("cab"), rs.getString("group"), rs.getString("teacher"), rs.getString("subject"));
        } catch (NullPointerException | SQLException e) {
            System.out.println(e);
            return null;
        }
    }

    public static TimeTeacherGroupWrapper reportWrap(Lesson lesson) {
        if (lesson.getId() == null) {
            return null;
        }
        try {
            String query = "select group_name as group, time_start as beg, time_end as end, teachers.name as teacher " +
                    "from lessons l " +
                    "join groups on l.group_id = groups.id " +
                    "join timeslots on l.timeslot_id = timeslots.id " +
                    "join teachers on l.teacher_id = teachers.id " +
                    "where l.id = " + lesson.getId();
            ResultSet rs = Lesson.execute(query);
            if (rs != null) {
                rs.next();
                TimeTeacherGroupWrapper wrapper = new TimeTeacherGroupWrapper(rs.getString("beg") + " - " + rs.getString("end"), rs.getString("group"), rs.getString("teacher"));
                return wrapper;
            }
        } catch (NullPointerException | SQLException e) {
            System.out.println(e);
            return null;
        }
        return null;
    }
}
