package ParametersGetter;


import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParametersGetter {
    /**
     * get entity id from url
     * @param request - handled request
     * @return - String id
     */
    public static String getId(HttpServletRequest request) {
        Pattern pattern = Pattern.compile("lessons/(\\d+)");
        Matcher matcher = pattern.matcher(request.getRequestURI());
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    public static String getTimeSlotId(String body) {
        Pattern pattern = Pattern.compile("\"timeslot_id\"\r\n\r\n(\\d+)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }


    public static String getGroupId(String body) {
        Pattern pattern = Pattern.compile("\"group_id\"\r\n\r\n(\\d+)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    public static String getAudienceId(String body) {
        Pattern pattern = Pattern.compile("\"audience_id\"\r\n\r\n(\\d+)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }


    public static String getTeacherId(String body) {
        Pattern pattern = Pattern.compile("\"teacher_id\"\r\n\r\n(\\d+)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }


    public static String getSubjectId(String body) {
        Pattern pattern = Pattern.compile("\"subject_id\"\r\n\r\n(\\d+)");
        Matcher matcher = pattern.matcher(body);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }


    public static String getBody(HttpServletRequest request) throws IOException {

        String body = null;
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;

        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                    stringBuilder.append(charBuffer, 0, bytesRead);
                }
            } else {
                stringBuilder.append("");
            }
        } catch (IOException ex) {
            throw ex;
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw ex;
                }
            }
        }
        body = stringBuilder.toString();
        return body;
    }
}
