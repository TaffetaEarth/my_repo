<%@ page import="DTO.LessonWrapper" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<% LessonWrapper[] lessons = (LessonWrapper[]) request.getAttribute("lessons"); %>
<table>
    <tr>
        <th>Время</th>
        <th>Группа</th>
        <th>Предмет</th>
        <th>Аудитория</th>
        <th>Учитель</th>
        <th>Сериализация</th>
        <th>Просмотр содержимого</th>
    </tr>
    <%for (LessonWrapper lesson: lessons){%>
        <tr>
                    <th><%= lesson.getTimeslot() %></th>
                    <th><%= lesson.getGroup() %></th>
                    <th><%= lesson.getSubject() %></th>
                    <th><%= lesson.getAudience() %></th>
                    <th><%= lesson.getTeacher() %></th>
                    <th><form action="/lessons/serialized_lessons/<%=lesson.getId()%>" method="post"><input type="submit" value="Сериализовать"></form></th>
                    <th><form action="/lessons/serialized_lessons/<%=lesson.getId()%>" method="get"><input type="submit" value="Посмотреть содержимое"></form></th>
        </tr>
<%}%>
</table>