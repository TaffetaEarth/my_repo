<%@ page import="DTO.TimeTeacherGroupWrapper" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<% TimeTeacherGroupWrapper[] wrappers = (TimeTeacherGroupWrapper []) request.getAttribute("wrappers"); %>
<% if (wrappers != null) { %>
    <% for (TimeTeacherGroupWrapper wrapper : wrappers) { %>
        <p>
        <%= wrapper.toString() %>
        </p>
        <% } %>
    <%}%>
<form action="/lessons/report" method="get">
    Group: <label>
    <input name="group"
           value="<%= request.getParameter("group") != null ? request.getParameter("group") : "" %>"
           placeholder="Введите название группы:"
    />
</label>
    <input type="submit" value="Send" onclick="request.setAttribute('group', group)"/>
</form>
</html>