package ru.sirius.main.program;


class Student{
   public static int studentsquan;
   private String name;
   private String group;
   Student(String name, String group){
     this.name = name;
     this.group = group;
     studentsquan++;
   }
   Student(){
     this.name = "_test_student_";
     this.group = "_test_group_";
     studentsquan++;
   }
   public void setGroup(String newgroup){
     this.group = newgroup;
   }
   public String getGroup(){
     return this.group;
   }
   public String getName(){
     return this.name;
   }
}
