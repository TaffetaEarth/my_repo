package ru.sirius.main.program;


class StudentsIterator implements java.util.Iterator<Student>{
  private int current = 0;
  private int limit = 10;
  private Student[] students;

  StudentsIterator(){
    this.students = new Student[limit];

    for (int i = 0;i < limit; i++) {
      students[i] = new Student("" + i, "" + i);
    }
  }

  public boolean hasNext(){
    return this.current <= this.limit;
  }

  public Student next(){
    Student value = new Student("" + this.current, "" + this.current);
    this.current++;
    return value;
  }
}
