package ru.sirius.main.program;


class Program{
  public static void main(String[] args) {
    StudentsIterator arr = new StudentsIterator();
    while(arr.hasNext()){
      Student s = arr.next();
      System.out.println(s.getName() + " "+ s.getGroup());
    }
  }
}
