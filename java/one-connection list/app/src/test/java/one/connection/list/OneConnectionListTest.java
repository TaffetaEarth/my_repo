package one.connection.list;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

class OneConnectionListTest{
  OneConnectionList<Integer> list;

  @BeforeEach public void set(){
  list = new OneConnectionList<Integer>();
  list.add(12);
  list.add(10);
  list.add(1);
  list.add(3);
  }

  @Test void fieldsTest(){
    assertEquals(list.head.getData(), 12);
    assertEquals(list.tail.getData(), 3);
  }

  @Test void toArrayObjectTest(){
    Object[] arr = {12, 10, 1, 3};
    Object[] listToArray = new Object[0];
    assertArrayEquals(arr, list.toArray(listToArray));
  }

  @Test void toArrayGenericTest(){
    Object[] arr = {12, 10, 1, 3};
    assertArrayEquals(arr, list.toArray());
  }

  @Test void iteratorTest(){
    Object[] arr = new Object[4];
    OneConnectionList.NodeStructureIterator iterator = list.iterator();
    int i = 0;
    while (iterator.hasNext()) {
      arr[i] = iterator.next();
      i += 1;
    }
    assertArrayEquals(arr, list.toArray());
  }

  @Test void addTest(){
    Object[] arr = {12, 10, 1, 3, 7};
    list.add(7);
    assertArrayEquals(arr, list.toArray());
  }

  @Test void addAllTest(){
    Object[] arr = {12, 10, 1, 3, 7, 14, 19, 100};
    ArrayList<Integer> newList = new ArrayList<Integer>();
    newList.add(7);
    newList.add(14);
    newList.add(19);
    newList.add(100);
    list.addAll(newList);
    assertArrayEquals(arr, list.toArray());
  }

  @Test void sizeTest(){
    assertEquals(4, list.size());
  }

  @Test void clearTest(){
    Object[] arr = {};
    list.clear();
    assertArrayEquals(arr, list.toArray());
  }

  @Test void containsTest(){
    assertTrue(list.contains(1));
    assertFalse(list.contains(6));
  }

  @Test void containsAllTest(){
    ArrayList<Integer> newList = new ArrayList<Integer>();
    newList.add(3);
    newList.add(1);
    newList.add(10);
    assertTrue(list.containsAll(newList));
  }

  @Test void isEmptyTest(){
    list = new OneConnectionList<Integer>();
    assertTrue(list.isEmpty());
  }

  @Test void removeTest(){
    Object[] arr = {12, 10, 3};
    list.remove(1);
    assertArrayEquals(arr, list.toArray());
  }

  @Test void removeAllTest(){
    ArrayList<Integer> newList = new ArrayList<Integer>();
    Object[] arr = {12};
    newList.add(3);
    newList.add(1);
    newList.add(10);
    list.removeAll(newList);
    assertArrayEquals(arr, list.toArray());
  }

  @Test void equalsTest(){
    OneConnectionList<Integer> newList = new OneConnectionList<Integer>();
    newList.add(12);
    newList.add(10);
    newList.add(1);
    newList.add(3);
    assertTrue(list.equals(newList));
  }
}
