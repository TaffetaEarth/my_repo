package one.connection.list;

class App{
  public static void main(String[] args) {
    OneConnectionList<Integer> list = new OneConnectionList<Integer>();
    OneConnectionList<Integer> elist = new OneConnectionList<Integer>();
    list.add(10);
    list.add(8);
    list.add(25);
    elist.add(10);
    elist.add(8);
    elist.add(25);
    Object[] arr = list.toArray();
    list.printAll();
    elist.printAll();
    System.out.println(list.equals(elist));
  }
}
