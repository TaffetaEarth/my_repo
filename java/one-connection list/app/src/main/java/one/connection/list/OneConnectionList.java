package one.connection.list;
import java.util.Collection;


class OneConnectionList<T> implements Collection{
  public Node head = null;
  public Node tail = null;

  class Node{
    private Node next = null;
    private T data = null;

    Node(){
      this.next = null;
      this.data = null;
    }

    Node(T data){
      this.next = null;
      this.data = data;
    }

    public T getData(){
      return this.data;
    }

    public Node getNext(){
      return this.next;
    }

    public void setData(T newData){
      this.data = newData;
    }

    public void setNext(Node newNext){
      this.next = newNext;
    }
  }

  OneConnectionList(){
  }

  public Object[] toArray(Object[] a){
    int size = this.size();
    a = new Object[size];
    Node node = this.head;
    for (int i = 0; i < size; i++) {
      a[i] = node.getData();
      node = node.getNext();
    }
    return a;
  }

  public T[] toArray(){
    int size = this.size();
    T[] arr = (T[]) new Object[size];
    Node node = this.head;
    for (int i = 0; i < size; i++) {
      arr[i] = node.getData();
      node = node.getNext();
    }
    return arr;
  }

  interface NodeStructureIterator extends java.util.Iterator { }

  private class NodeIterator implements NodeStructureIterator{

    Node currentNode = head;

    public boolean hasNext(){
      return currentNode != null;
    }

    public T next(){
      T result = currentNode.getData();
      currentNode = currentNode.getNext();
      return result;
    }
  }


  public NodeStructureIterator iterator(){
    NodeStructureIterator iterator = this.new NodeIterator();
    return iterator;
  }

  public void printAll(){
    NodeStructureIterator iterator = this.new NodeIterator();
    while (iterator.hasNext()) {
      System.out.print(iterator.next() + " ");
    }
      System.out.println();
  }


  public boolean add(Object data){
    try{
      Node node = new Node((T)data);
      if(this.head == null){
        this.head = node;
        this.tail = node;
      } else {
        this.tail.setNext(node);
        this.tail = node;
      }
    } catch (Exception e){
      return false;
    }
    return true;
  }


  public boolean addAll(Collection collection){
    T[] arr = (T[]) collection.toArray();
    for (int i = 0; i < arr.length; i++) {
      this.add(arr[i]);
    }
    return true;
  }


  public int size(){
    Node node = this.head;
    int size = 0;
    while (node != null) {
      size++;
      node = node.getNext();
    }
    return size;
  }


  public void clear(){
    Node next = new Node();
    while (this.head != null) {
      next = this.head.getNext();
      this.head.setData(null);
      this.head.setNext(null);
      this.head = next;
    }
  }


  public boolean contains(Object o){
    Node node = this.head;
    while (node != null){
      if(node.getData() == o){
        return true;
      }
      node = node.getNext();
    }
    return false;
  }

  public boolean containsAll(Collection collection){
    Node node = this.head;
    int counter = 0;
    T[] arr = (T[]) collection.toArray();
    for (int i = 0; i < arr.length; i++){
      if (this.contains(arr[i])){
        counter++;
      }
    }
    if (counter == arr.length) {
      return true;
    } else {
      return false;
    }
  }


  public boolean isEmpty(){
    if (this.size() == 0){
      return true;
    } else {
      return false;
    }
  }


  public boolean remove(Object o){
    Node node = this.head;
    Node next = new Node();
    try{
      while (node != null){
        if (node.getNext().getData() == o){
          next = node.getNext();
          node.setNext(next.getNext());
          next.setData(null);
          next.setNext(null);
          return true;
        }
        node = node.getNext();
      }
    } catch (Exception e){
      System.out.println(e);
      return false;
    }
    return false;
  }

  public boolean removeAll(Collection collection){
    try{
      T[] arr = (T[]) collection.toArray();
      for (int i = 0; i < arr.length; i++) {
        this.remove(arr[i]);
      }
      return true;
    }catch (Exception e) {
      return false;
    }
  }

  public boolean equals(Object o){
    if (!(o instanceof OneConnectionList)){
      System.out.println("Other class");
      return false;
    }
    OneConnectionList newList = (OneConnectionList) o;
    if(this.size() != newList.size()){
      System.out.println("Other size");
      return false;
    }
    Node node = this.head;
    Node newNode = newList.head;
    while (node != null) {
      if (node.getData() != newNode.getData()) {
        return false;
      }
      node = node.getNext();
      newNode = newNode.getNext();
    }
    return true;
  }

  public boolean retainAll(Collection c){
    throw new UnsupportedOperationException();
  }
}
