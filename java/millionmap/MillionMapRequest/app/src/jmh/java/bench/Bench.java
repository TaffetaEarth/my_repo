package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import MillionMapRequest.*;

public class Bench {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Generations {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;
        static String[] keys;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
            keys = new String[App.CHECK_SIZE];
        }


        @Benchmark
        public static void checkGenerationHashMap() {
            App.generation(hm);
        }

        @Benchmark
        public static void checkGenerationLinkedHashMap() {
            App.generation(lhm);
        }

        @Benchmark
        public static void checkGenerationTreeMap() {
            App.generation(tm);
        }

        @Benchmark
        public static void checkGenerationHashtable() {
            App.generation(ht);
        }

        @Benchmark
        public static void checkKeysGeneration(){App.keysGeneration(keys);}

    }


    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Maps {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;
        static Map<String, Integer> sums;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
            sums = new HashMap<String, Integer>();
        }


        @Benchmark
        public static void checkLinkedHashMap() {
            App.checkLinkedHashMap(sums);
        }

        @Benchmark
        public static void checkHashMap() {
            App.checkHashMap(sums);
        }

        @Benchmark
        public static void checkHashtable() {
            App.checkHashtable(sums);
        }

        @Benchmark
        public static void checkTreeMap() {
            App.checkTreeMap(sums);
        }

    }
}