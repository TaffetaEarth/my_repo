12.Задания на изерение времени различных операций различных видов Map

Задачи:

1. Создать отображение размером в 2 000 000 записей.
   Ключ - строка из 10 случайных символов.
   Значение - случайное число.
   Вывести значения отсортированные по возрастанию.
   Сравнить скорость работы для разных базовых классов.


2. Создать отображение размером в 2 000 000 записей.
Ключ - строка из 10 случайных символов.
Значение - случайное число.
Создать массив из 30 000 строк длиной 10 случайных символов. Найти, сумму значений отображения, соответствующих строкам из массива. Запрещено создавать дополнительные структуры данных.
Сравнить скорость работы для разных базовых классов.

Результаты п.1
|Benchmark                                     |  Mode | Cnt | Score   Error |Units|
|----------------------------------------------|-------|-----|---------------|-----|
|Bench.Generations.checkGenerationHashMap      |  avgt |   5 | 2.610 ± 0.217 | s/op|
|Bench.Generations.checkGenerationHashtable    |  avgt |   5 | 2.268 ± 1.561 | s/op|
|Bench.Generations.checkGenerationLinkedHashMap|  avgt |   5 | 2.448 ± 0.157 | s/op|
|Bench.Generations.checkGenerationTreeMap      |  avgt |   5 |4.539 ± 0.203 | s/op|
|Bench.Maps.checkHashMap                       |  avgt |   5 |2.723 ± 0.369 | s/op|
|Bench.Maps.checkHashtable                     |  avgt |  5 |2.703 ± 0.119  |s/op|
|Bench.Maps.checkLinkedHashMap                 |  avgt |   5 |2.561 ± 0.164  |s/op|
|Bench.Maps.checkTreeMap                       |  avgt |   5 |4.568 ± 0.827  |s/op|
|Bench.Sorts.checkQuickSortHashMap             |  avgt |   5 |0.770 ± 0.118  |s/op|
|Bench.Sorts.checkQuickSortHashtable           |  avgt |   5 |0.765 ± 0.073  |s/op|
|Bench.Sorts.checkQuickSortLinkedHashMap       |  avgt |   5 |0.687 ± 0.080  |s/op|
|Bench.Sorts.checkQuickSortTreeMap             |  avgt |   5 |0.735 ± 0.209  |s/op|


Результаты п.2
|Benchmark                                       |Mode  |Cnt  |Score   Error |Units|
|-----------------------------------------------|-------|-----|----------------|-----|
|Bench.Generations.checkGenerationHashMap      |  avgt  |  5 | 2.625 ± 0.306|   s/op|
|Bench.Generations.checkGenerationHashtable    |  avgt  |  5 | 2.323 ± 1.520|   s/op|
|Bench.Generations.checkGenerationLinkedHashMap|  avgt  |  5 | 2.511 ± 0.138|   s/op|
|Bench.Generations.checkGenerationTreeMap      |  avgt  |  5 | 4.893 ± 1.169|  s/op|
|Bench.Generations.checkKeysGeneration         |  avgt  |  5 | 0.015 ± 0.001|   s/op|
|Bench.Maps.checkHashMap                       |  avgt  |  5 | 0.015 ± 0.003|   s/op|
|Bench.Maps.checkHashtable                     |  avgt  |  5 | 0.014 ± 0.003|   s/op|
|Bench.Maps.checkLinkedHashMap                 |  avgt  |  5 | 0.015 ± 0.001|  s/op|
|Bench.Maps.checkTreeMap                       |  avgt  |  5 | 0.014 ± 0.001|   s/op|
