package MillionMap;

import java.util.Collection;
import java.util.Map;

/**
 * @author TaffetaEarth
 * @version 1.0
 */
public class ParametersGetter {
    /**
     * method to get values from Map
     * @param map - map with values
     * @return - array of Integer with values from map
     */
    public static Integer[] getValues(Map map){
        Collection val = map.values();
        return (Integer[]) val.toArray(new Integer[0]);
    }
}
