package MillionMap;

import java.util.*;
import java.time.*;

public class App {
    /**
     * count of pairs key-value in the maps
     */
    public static final int MAP_SIZE = 2_000_000;
    /**
     * count of program retries
     */
    public static final int COUNT_OF_TRIES = 10;
    public static void main(String[] args) {
        Map<String, ArrayList<Double>> result = new HashMap<String, ArrayList<Double>>();
        Map<String, ArrayList<Double>> gen = new HashMap<String, ArrayList<Double>>();
        Map<String, ArrayList<Double>> sort = new HashMap<String, ArrayList<Double>>();
        Map<String, ArrayList<Double>> get = new HashMap<String, ArrayList<Double>>();
        Map[] maps = {result, gen, get, sort};
        for (int i = 0; i < maps.length; i++) {
            maps[i].put("class java.util.HashMap", new ArrayList<Double>());
            maps[i].put("class java.util.TreeMap", new ArrayList<Double>());
            maps[i].put("class java.util.Hashtable", new ArrayList<Double>());
            maps[i].put("class java.util.LinkedHashMap", new ArrayList<Double>());
        }
        for (int quant = 0; quant < COUNT_OF_TRIES; quant++) {
            HashMap<String, Integer> hm = new HashMap<String, Integer>();
            LinkedHashMap<String, Integer> lhm = new LinkedHashMap<String, Integer>();
            TreeMap<String, Integer> tm = new TreeMap<String, Integer>();
            Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
            Map[] arr = {hm, lhm, tm, ht};
            for (int i = 0; i < arr.length; i++) {
                Instant startGenTime = Instant.now();
                generation(arr[i]);
                Instant endGenTime = Instant.now();
                gen.get(arr[i].getClass().toString()).add((double) Duration.between(startGenTime, endGenTime).toMillis());
            }
            for (int i = 0; i < arr.length; i++) {
                Instant startTime = Instant.now();
                Collection val = arr[i].values();
                Integer[] values = (Integer[]) val.toArray(new Integer[0]);
                Instant endGetTime = Instant.now();

                get.get(arr[i].getClass().toString()).add((double) Duration.between(startTime, endGetTime).toMillis());
                Instant startSortTime = Instant.now();
                Sort.quickSort(values, 0, values.length - 1);
                Instant endSortTime = Instant.now();
                sort.get(arr[i].getClass().toString()).add((double) Duration.between(startSortTime, endSortTime).toMillis());
                Instant endTime = Instant.now();
                result.get(arr[i].getClass().toString()).add((double) Duration.between(startTime, endTime).toMillis());
            }
        }
        System.out.println("class java.util.HashMap"
                + "\nResult\n" + av(result.get("class java.util.HashMap").toArray(new Double[0]))
                + "\nGen\n" + av(gen.get("class java.util.HashMap").toArray(new Double[0]))
                + "\nGet\n" + av(get.get("class java.util.HashMap").toArray(new Double[0]))
                + "\nSort\n" + av(sort.get("class java.util.HashMap").toArray(new Double[0]))
                + "\n\nclass java.util.TreeMap"
                + "\nResult\n" + av(result.get("class java.util.TreeMap").toArray(new Double[0]))
                + "\nGen\n" + av(gen.get("class java.util.TreeMap").toArray(new Double[0]))
                + "\nGet\n" + av(get.get("class java.util.TreeMap").toArray(new Double[0]))
                + "\nSort\n" + av(sort.get("class java.util.TreeMap").toArray(new Double[0]))
                + "\n\nclass java.util.Hashtable"
                + "\nResult\n" + av(result.get("class java.util.Hashtable").toArray(new Double[0]))
                + "\nGen\n" + av(gen.get("class java.util.Hashtable").toArray(new Double[0]))
                + "\nGet\n" + av(get.get("class java.util.Hashtable").toArray(new Double[0]))
                + "\nSort\n" + av(sort.get("class java.util.Hashtable").toArray(new Double[0]))
                + "\n\nclass java.util.LinkedHashMap"
                + "\nResult\n" + av(result.get("class java.util.LinkedHashMap").toArray(new Double[0]))
                + "\nGen\n" + av(gen.get("class java.util.LinkedHashMap").toArray(new Double[0]))
                + "\nGet\n" + av(get.get("class java.util.LinkedHashMap").toArray(new Double[0]))
                + "\nSort\n" + av(sort.get("class java.util.LinkedHashMap").toArray(new Double[0]))
        );
    }

    /**
     * method to check time of generation and sort on TreeMap
     */
    public static void checkTreeMap() {
        TreeMap<String, Integer> tm = new TreeMap<String, Integer>();
        generation(tm);
        Integer[] values = ParametersGetter.getValues(tm);
        Sort.quickSort(values, 0, values.length - 1);
    }

    /**
     * method to check time of generation and sort on HashMap
     */
    public static void checkHashMap() {
        HashMap<String, Integer> hm = new HashMap<String, Integer>();
        generation(hm);
        Integer[] values = ParametersGetter.getValues(hm);
        Sort.quickSort(values, 0, values.length - 1);
    }

    /**
     * method to check time of generation and sort on Hashtable
     */
    public static void checkHashtable() {
        Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
        generation(ht);
        Integer[] values = ParametersGetter.getValues(ht);
        Sort.quickSort(values, 0, values.length - 1);
    }

    /**
     * method to check time of generation and sort on LinkedHashMap
     */
    public static void checkLinkedHashMap() {
        LinkedHashMap<String, Integer> lhm = new LinkedHashMap<String, Integer>();
        generation(lhm);
        Integer[] values = ParametersGetter.getValues(lhm);
        Sort.quickSort(values, 0, values.length - 1);
    }

    /**
     * method to generate pairs key-value in map
     * @param map - map to fill
     */
    public static void generation(Map map) {
        for (int i = 0; i < MAP_SIZE; i++) {
            String key = "";
            for (int j = 0; j < 10; j++) {
                key += (char) new Random().nextInt(127);
            }
            map.put(key, new Random().nextInt(Integer.MAX_VALUE));
        }
    }


    /**
     * method to count the average value of array of Double values
     * @param arr - array to fill
     * @return - average value of array
     */

    public static double av(Double[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return ((double) sum / arr.length) / 1000;
    }



}
