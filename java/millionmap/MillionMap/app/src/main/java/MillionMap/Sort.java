package MillionMap;

/**
 * @author TaffetaEarth
 * @version 1.0
 */
public class Sort {
    /**
     * method to sort array of Integer by quicksort
     * @param arr - array to sort
     * @param begin - start border of sort
     * @param end - end border of sort
     */

    public static void quickSort(Integer[] arr, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            quickSort(arr, begin, partitionIndex-1);
            quickSort(arr, partitionIndex+1, end);
        }
    }

    /**
     * part of a quicksort algorithm
     * @param arr - part of array to sort
     * @param begin - start border of sort
     * @param end - end border of sort
     * @return - index of element
     */
    private static int partition(Integer[] arr, int begin, int end) {
        int pivot = arr[end];
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;

                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        int swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;

        return i+1;
    }
}
