package bench;

import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.*;


import MillionMap.*;

public class Bench {
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Generations {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
        }


        @Benchmark
        public static void checkGenerationHashMap() {
            App.generation(hm);
        }

        @Benchmark
        public static void checkGenerationLinkedHashMap() {
            App.generation(lhm);
        }

        @Benchmark
        public static void checkGenerationTreeMap() {
            App.generation(tm);
        }

        @Benchmark
        public static void checkGenerationHashtable() {
            App.generation(ht);
        }

    }


    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Sorts {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            hm = new HashMap<String, Integer>();
            lhm = new LinkedHashMap<String, Integer>();
            tm = new TreeMap<String, Integer>();
            ht = new Hashtable<String, Integer>();
            App.generation(hm);
            App.generation(lhm);
            App.generation(tm);
            App.generation(ht);
        }

        @Benchmark
        public static void checkQuickSortHashMap() {
            Integer[] values = ParametersGetter.getValues(hm);
            Sort.quickSort(values, 0, values.length - 1);
        }

        @Benchmark
        public static void checkQuickSortLinkedHashMap() {
            Integer[] values = ParametersGetter.getValues(lhm);
            Sort.quickSort(values, 0, values.length - 1);
        }

        @Benchmark
        public static void checkQuickSortTreeMap() {
            Integer[] values = ParametersGetter.getValues(tm);
            Sort.quickSort(values, 0, values.length - 1);
        }

        @Benchmark
        public static void checkQuickSortHashtable() {
            Integer[] values = ParametersGetter.getValues(ht);
            Sort.quickSort(values, 0, values.length - 1);
        }
    }

    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Measurement(iterations = 5)
    @Warmup(iterations = 1)
    @Fork(value = 1)
    @State(Scope.Benchmark)
    public static class Maps {
        static HashMap<String, Integer> hm;
        static LinkedHashMap<String, Integer> lhm;
        static TreeMap<String, Integer> tm;
        static Hashtable<String, Integer> ht;

        @Setup(Level.Iteration)
        public void setup() {
            Maps.hm = new HashMap<String, Integer>();
            Maps.lhm = new LinkedHashMap<String, Integer>();
            Maps.tm = new TreeMap<String, Integer>();
            Maps.ht = new Hashtable<String, Integer>();
        }


        @Benchmark
        public static void checkLinkedHashMap() {
            App.checkLinkedHashMap();
        }

        @Benchmark
        public static void checkHashMap() {
            App.checkHashMap();
        }

        @Benchmark
        public static void checkHashtable() {
            App.checkHashtable();
        }

        @Benchmark
        public static void checkTreeMap() {
            App.checkTreeMap();
        }

    }
}