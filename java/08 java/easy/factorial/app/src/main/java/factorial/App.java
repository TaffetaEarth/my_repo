/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package factorial;

class Factorial{
	public static void main(String[] args){
		factorial(10);
	}


	public static long factorial(int max){
	long a = 1;
	for(int i = 1; i <= max; i++){
		a = a*i;
		}
	return a;
	}
}
