package matrix;


class SqMatrix{
  public static void main(String[] args) {
    int n = 6;
    int m = n;
    Matrix matrix = new Matrix(n, m);
    for (int i = 0;i < n; i++) {
      for (int j = 0;j < m; j++) {
        if (j <= i) {
          System.out.print(matrix.content[i][j] + "\t");
        }else{
          System.out.print("\t");
        }
      }
    System.out.println();
    }
  }
}
