package matrix;

import java.util.Scanner;

class Matrixtrans{
  public static void main(String[] args){

  }
  public static int[][] MatrixGeneration(int m, int n){
    int[][] matrix = new int[n][m];
    for (int i = 0; i < n; i++){
      for (int j = 0; j < m; j++){
        matrix[i][j] = (int) (Math.random() * 10);
      }
    }
    return matrix;
  }
  public static void MatrixOutput(int m, int n, int[][] matrix){
    for (int i = 0; i < n; i++){
      for (int j = 0; j < m; j++){
        System.out.print(matrix[i][j] + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }
  public static void MatrixOutputTransposition(int m, int n, int[][] matrix){
    String str = "";
    for (int i = 0; i < m; i++){
      for (int j = 0; j < n; j++){
        System.out.print(matrix[j][i] + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }
}
