package matrix;


class LineMatrix{
  public static void main(String[] args) {
    int n = 6;
    int m = 5;
    Matrix matrix = new Matrix(n, m);
    System.out.println(lineOutput(matrix));
  }
  public static String lineOutput(Matrix matrix){
    String str = "";
    for (int i = 0;i < matrix.n; i++) {
      for (int j = 0;j < matrix.m; j++) {
        if (matrix.content == null){
          return "Null matrix";
        }
      }
    }
    for (int i = 0;i < matrix.n; i++) {
      for (int j = 0;j < matrix.m; j++) {
        str += matrix.content[i][j];
        str += " ";
      }
    }
    return str;
  }
}
