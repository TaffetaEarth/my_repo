package matrix;


class Matrix{
  public int n;
  public int m;
  public int[][] content;

  Matrix(int n, int m){
    this.n = n;
    this.m = m;
  }

  public void MatrixGeneration(){
    this.content = new int[this.n][this.m];
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        this.content[i][j] = (int) (Math.random() * 10);
      }
    }
  }

  public void MatrixOutput(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        System.out.print(this.content[i][j] + " ");
      }
      System.out.println();
    }
    System.out.println();
  }
}
