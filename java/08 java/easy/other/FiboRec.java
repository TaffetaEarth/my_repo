package Fibo;


class FiboRec{
	public static void main(String[] args){
		System.out.println(fiboOpti(550));
}
	public static long recFibo(long a){
	if(a == 0){
		return 0;}
	if(a == 1){
		return 1;}
	else{
		return recFibo(a-1)+recFibo(a-2);
		}
	}

	public static long fiboOpti(int n){
		int arr[] = new int[n+1];
		arr[0] = 0;
		arr[1] = 1;
		for (int i = 2; i <= n; i++){
			arr[i] = arr[i-1] + arr[i-2];
		}
		return arr[n];
	}
}
