/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package NumbersGeneration;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class NumbersGenerationTest {
    @Test void testGenerationGood() {
      Generator gen = new Generator(100, 5);
      assertTrue(gen.getSucsessful());
      assertEquals(100, gen.getNumbers().length);
    }

    @Test void testGenerationBad(){
      Generator gen = new Generator(1000000, 5);
      assertNull(gen.getNumbers());
    }
}
