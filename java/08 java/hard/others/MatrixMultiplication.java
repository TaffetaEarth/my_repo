package gradle08.java.hard.src.ru.sirius.main.program;
import java.util.Scanner;

class MatrixMultiplication{
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int quan = scan.nextInt();
    int n = scan.nextInt();
    Matrix result = new Matrix();
    for (int i = 0; i < quan; i++){
      if(result.content == null){
        result = new Matrix(n, n);
        result.matrixGeneration();
        continue;
      }
      Matrix matrix = new Matrix(n, n);
      matrix.matrixGeneration();
      result = result.matrixMultiplication(matrix);
    }
    result.matrixOutput();
  }
}
