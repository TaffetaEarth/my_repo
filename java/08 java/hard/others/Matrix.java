package gradle08.java.hard.src.ru.sirius.main.program;
import java.util.Random;
import java.io.*;

class Matrix implements Serializable{
  public int n;
  public int m;
  public int[][] content;
  public int threadsnum;
  public int[] slices;
  private static final long serialVersionUID = 1L;

  Matrix(){}

  Matrix(int n, int m){
    this.n = n;
    this.m = m;
    this.content = new int[n][m];
  }

  Matrix(int[][] content){
    this.n = content.length;
    this.m = content[0].length;
    this.content = content;
  }

  Matrix(int n, int m, int[][] content){
    this.n = n;
    this.m = m;
    this.content = content;
  }

  public void matrixGeneration(){
    int[][] matrix = new int[this.n][this.m];
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        matrix[i][j] = new Random().nextInt(10);
      }
    }
    this.content = matrix;
  }

  public void matrixOutput(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        System.out.print(this.content[i][j] + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }

  public Matrix matrixMultiplication(Matrix m2){
    boolean flag = false;
    int dim = 0;
    if (this.n == m2.m){
      dim = this.n;
      flag = true;
    }
    if (flag == false){
      return null;
    }
    Matrix result = new Matrix(dim, dim);
    for (int o = 0; o < dim; o++) {
      for (int k = 0; k < dim; k++) {
        for (int i = 0; i < m2.n; i++) {
          int counter = 0;
          for (int j = 0; j < dim; j++) {
            counter += this.content[o][j] * m2.content[j][i];
          }
          result.content[o][k] = counter;
          k++;
        }
      }
    }
    return result;
  }


  public void matrixOutputStarsSky(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        String space = "";
        for (int k = 0; k < 2 + new Random().nextInt(3); k++) {
          space += " ";
        }
        System.out.print(this.content[i][j] + space);
        try {
          Thread.sleep(2);
        } catch (Exception e) {}
      }
    }
  }

  public void matrixDividing(int threadsnum){
    this.threadsnum = threadsnum;
    Matrix[] resultarr = new Matrix[threadsnum];
    int n = this.n;
    int j = 0;
    int[] lenArr = new int[threadsnum];
    while (j < n){
      for (int i = 0; i < threadsnum; i++) {
        if (j < n){
          lenArr[i]++;
          j++;
        }
      }
    }
    this.slices = lenArr;
  }

  public int[][] getContent(){
    return this.content;
  }
}
