package others;
import java.util.Scanner;
import java.util.Date;
import java.util.Random;


class Distances{
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int repeatnum = scan.nextInt();
    int areasize = scan.nextInt();
    int swipe = scan.nextInt();
    int i = 0;
    int counter = 0;
    while (i < repeatnum) {
      int first = swipe + new Random().nextInt(areasize);
      int second = swipe + new Random().nextInt(areasize);
      int third = swipe + new Random().nextInt(areasize);
      int fourth = swipe + new Random().nextInt(areasize);
      if ((first < third && third < second) || (first < fourth && fourth < second) ||
       (third < first && first < fourth) || (third < second && second < fourth)){
        counter++;
      }
      i++;
    }
    System.out.println(counter);
  }
}
