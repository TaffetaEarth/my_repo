package others.menu;
import java.util.Scanner;
import java.util.Date;
import others.menu.actions.*;

class MenuTree{
  public static void main(String[] args) {
    Question zero = new Question();
    Question first1 = new Question();
    Question first2 = new Question();
    Question second1 = new Question();
    Question second2 = new Question();
    Action hello = new ActionPrintHelloWorld("Вывести Hello, world");
    zero.connect(first1);
    zero.connect(first2);
    zero.setAction(hello);

    first1.setAction(new ActionPrintPi("Вывести P"));
    first1.connect(second1);
    first1.connect(second2);

    first2.setAction(new ActionPrintE("Вывести Е"));

    second1.setAction(new ActionPrintName("Вывести имя"));
    second2.setAction(new ActionPrintDate("Вывести дату"));

    menuStart(zero);
  }

  public static void menuStart(Question start) {
    Scanner scan = new Scanner(System.in);
    int answ = 0;
    boolean flag = true;
    // System.out.println(answ);
    //   }
    // }
    // }
    // }
    // }
    while(flag){
      start.printDescription();
      answ = scan.nextInt();
      if (answ == 0){
         if (start.parent == null){
              flag = false;
        } else {
          start = start.parent;
        }
      }
      if(answ > 0 && answ <= start.actions.size()){
        start.actions.get(answ - 1).act();
        System.out.println("1");
        }
      if(answ > start.actions.size() && answ <= start.actions.size() + start.children.size()) {
          start = start.children.get(answ - start.children.size());
          System.out.println("2");
        }
      if(answ > start.actions.size() + start.children.size()){
          System.out.println("Введите корректное значение");
        }
    }
  }
}
