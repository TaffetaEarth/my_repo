package others.menu.actions;

public class ActionPrintE extends Action{
  String description = "Вывести E";

  public ActionPrintE(String description) {
    super(description);
  }

  public void act(){
    System.out.println("\n" + Math.E + "\n");
  }
}
