package others.menu.actions;

import java.util.Calendar;

public class ActionPrintDate extends Action{
  String description = "Вывести дату";

  public ActionPrintDate(String description) {
    super(description);
  }

  public void act(){
    System.out.println("\n" + Calendar.DATE + "\n");
  }
}
