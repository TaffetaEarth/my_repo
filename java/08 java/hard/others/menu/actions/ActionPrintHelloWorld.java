package others.menu.actions;


public class ActionPrintHelloWorld extends Action{

  public ActionPrintHelloWorld(String description) {
    super(description);
  }

  public void act(){
    System.out.println("Hello, World!");
  }
}
