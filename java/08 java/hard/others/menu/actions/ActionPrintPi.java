package others.menu.actions;
public class ActionPrintPi extends Action{
  String description = "Вывести P";

  public ActionPrintPi(String description) {
    super(description);
  }

  public void act(){
    System.out.println(Math.PI);
  }
}
