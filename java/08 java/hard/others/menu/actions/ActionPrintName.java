package others.menu.actions;
import java.util.Scanner;

public class ActionPrintName extends Action{
  String description = "Вывести имя";

  public ActionPrintName(String description) {
    super(description);
  }

  public void act(){
    Scanner scan = new Scanner(System.in);
    String name = scan.next();
    System.out.println("\n Hello, "  + name + "! \n");
  }
}
