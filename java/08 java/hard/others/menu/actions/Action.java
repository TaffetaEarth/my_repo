package others.menu.actions;


abstract public class Action{
  String description;

  protected Action(String description){
    this.description = description;
  }

  abstract public void act();

  public String getDescription(){
    return this.description;
  }
}
