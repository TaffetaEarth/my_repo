package gradle08.java.hard.src.ru.sirius.main.program;
import java.util.Random;
import java.util.Scanner;
import java.util.Date;

class Modelling{
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    double stop = scan.nextDouble();
    double coef = scan.nextDouble();
    long maxtime = scan.nextLong();
    Thread tread = new Thread();
    int keygen = (int) stop;
    long starttime = new Date().getTime();
    long end = starttime;
    do{
      try{
        tread.sleep(new Random().nextInt(1000));
      } catch (Exception e) {}
      keygen = (int) ((-1) * (coef*stop) + 2*(new Random().nextInt((int) (coef*stop))));
      end = new Date().getTime();
      if (end - starttime > maxtime){
        break;
      }
      System.out.println("" + keygen + " " + (end - starttime));
    } while ((Math.abs(keygen) <= Math.abs(stop)) && (end - starttime <= maxtime));
  }
}
