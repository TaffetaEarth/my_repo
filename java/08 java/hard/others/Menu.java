package gradle08.java.hard.src.ru.sirius.main.program;
import java.util.Scanner;
import java.util.Date;

class Menu{
  public static void main(String[] args) {
    menu(0);
  }


  public static int menu(int level){
    Scanner scan = new Scanner(System.in);
    int answer = 0;
    if (level == 0){
      System.out.println("Вы находитесь в меню. Выберите вариант:");
      System.out.println("0. Выход");
      System.out.println("1. Вывести время");
      System.out.println("2. Следующий уровень");
      answer = scan.nextInt();
      if(answer == 0){
        return 0;
      }
      if(answer == 1){
        menu(1);
      }
      if(answer == 2){
        menu(2);
      }
    }
    if (level == 1){
      System.out.println(new Date().getTime());
      System.out.println();
      menu(0);
    }
    if (level == 2){
      System.out.println("Вы находитесь в меню. Выберите вариант:");
      System.out.println("0. Назад");
      System.out.println("1. Вывести число П");
      System.out.println("2. Следующий уровень");
      answer = scan.nextInt();
      if(answer == 0){
        menu(0);
      }
      if(answer == 1){
        menu(3);
      }
      if(answer == 2){
        menu(4);
      }
    }
    if (level == 3){
      System.out.println(Math.PI);
      System.out.println();
      menu(2);
    }
    if (level == 4){
      System.out.println("Вы находитесь в меню. Выберите вариант:");
      System.out.println("0. Назад");
      System.out.println("1. Вывести число E");
      System.out.println("2. Выход");
      answer = scan.nextInt();
      if(answer == 0){
        menu(2);
      }
      if(answer == 1){
        menu(5);
      }
      if(answer == 2){
        return 0;
      }
    }
    if (level == 5){
      System.out.println(Math.E);
      System.out.println();
      menu(4);
    }
    return 0;
  }
}
