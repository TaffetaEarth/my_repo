package ru.sirius.main.program;

class Sum{
  public static void main(String[] args) {
    int a = 6;
    int b = 7;
    System.out.println(Sum.<Integer, Integer>sum(a, b));
  }
  public static <N, S> int sum(N a, S b){
    return a.hashCode()+b.hashCode();
  }
}
