package workers;

class Engineer extends Employee{
  public Engineer(){}

  public Engineer(String name, String place, int salary){
    super(name, place, salary);
  }

  public String toString(){
    String str = "My name is " + this.name
                + " \nMy place is " + this.place
                  + " \nMy salary is " + this.salary
                    + "I'm a engineer!";
    return str;
  }

  public void work(){
    System.out.println("I'm engineer and i'm making blueprint");
  }

  public void get_blueprint(){
    System.out.println("I'd created a blueprint!");
  }
}
