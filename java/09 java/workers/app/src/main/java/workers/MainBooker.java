package workers;

class MainBooker extends Employee{
  public MainBooker(){}

  public MainBooker(String name, String place, int salary){
    super(name, place, salary);
  }

  public String toString(){
    String str = "My name is " + this.name
                + " \nMy place is " + this.place
                  + " \nMy salary is " + this.salary
                    + "I'm a main booker!";
    return str;
  }

  public void work(){
    System.out.println("I'm mainbooker and i'm thinking about the most important things in this world");
  }

  public void setNewSalary(Employee e, int newSalary){
      e.setSalary(newSalary);
  }
}
