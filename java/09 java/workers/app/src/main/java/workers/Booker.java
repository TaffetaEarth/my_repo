package workers;

class Booker extends Employee{
  public Booker(){}

  public Booker(String name, String place, int salary){
    super(name, place, salary);
  }

  public String toString(){
    String str = "My name is " + this.name
                + " \nMy place is " + this.place
                  + " \nMy salary is " + this.salary
                    + "I'm a booker!";
    return str;
  }

  public void work(){
    System.out.println("I'm booker and i'm counting");
  }

  public void setNewSalary(Employee e, int newSalary){
    if (!(e instanceof MainBooker)){
      e.setSalary(newSalary);
    } else {
      System.out.println("I can't change my boss's salary ");
    }
  }
}
