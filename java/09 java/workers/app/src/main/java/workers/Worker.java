package workers;

class Worker extends Employee{
  public Worker(){}

  public Worker(String name, String place, int salary){
    super(name, place, salary);
  }

  public String toString(){
    String str = "My name is " + this.name
                + " \nMy place is " + this.place
                  + " \nMy salary is " + this.salary
                    + "I'm a worker!";
    return str;
  }

  public void work(){
    System.out.println("I'm worker and i'm working");
  }
}
