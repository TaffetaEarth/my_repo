package workers;


abstract class Employee{
  protected String name;
  protected String place;
  protected int salary;

  protected Employee(){}

  protected Employee(String name, String place, int salary){
    this.name = name;
    this.place = place;
    this.salary = salary;
  }

  protected Object clone(){
    try{
      return super.clone();
    } catch (Exception e) {
      return null;
    }
  }

  public boolean equals(Employee e){
    return this.hashCode() == e.hashCode();
  }

  public int hashCode(){
    int hash = 0;
    char[] arr = this.name.toCharArray();
    for (int i = 0; i < arr.length; i++) {
      hash += (int) arr[i];
    }
    arr = this.place.toCharArray();
    for (int i = 0; i < arr.length; i++) {
      hash += (int) arr[i];
    }
    hash += this.salary;
    return hash;
  }

  abstract public String toString();

  public String getName(){
    return this.name;
  }

  public String getPlace(){
    return this.place;
  }

  public int getSalary(){
    return this.salary;
  }

  public void setName(String name){
    this.name = name;
  }

  protected void setPlace(String place){
    this.place = place;
  }

  protected void setSalary(int salary){
    this.salary = salary;
  }
}
