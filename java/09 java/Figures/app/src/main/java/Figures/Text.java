package Figures;


class Text implements Outputable{
  public String name;
  public String description;
  public String content;

  Text(String name, String content){
    this.name = name;
    this.content = content;
  }

  Text(String name, String content, String description){
    this.name = name;
    this.content = content;
    this.description = description;
  }

  public String toString(){
    String res = "" + this.name + "\n" + this.content;
    return res;
  }


  public String getDescription(){
    return this.description;
  }
}
