package Figures;


class Triangle extends Polygon {
  Triangle(int sideLenght){
    super(sideLenght);
    this.sideNum = 3;
  }

  public String toString(){
    return "I am Triangle and my perimeter is " + this.getPerimeter();
  }
}
