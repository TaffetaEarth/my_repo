package Figures;


class Operation{

  public Outputable[] figures;

  Operation(Outputable[] figures){
    this.figures = figures;
  }

  public String[] output(){
    String[] arr = new String[this.figures.length];
    for (int i = 0; i < this.figures.length; i++) {
      arr[i] = this.figures[i].toString();
    }
    return arr;
  }
}
