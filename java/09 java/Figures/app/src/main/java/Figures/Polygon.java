package Figures;

abstract class Polygon implements Outputable{
  protected int sideNum;
  public int sideLenght;

  protected Polygon(int sideLenght){
    this.sideLenght = sideLenght;
  }


  public int getPerimeter(){
    return this.sideLenght * this.sideNum;
  }

  abstract public String toString();
}
