package Figures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class OperationTest{
  @Test void testOperation(){
    Outputable[] arr = new Outputable[5];
    for (int i =0; i < arr.length; i++) {
      arr[i] = new Triangle(5);
    }
    Operation o = new Operation(arr);
    assertDoesNotThrow(()->{
      o.output();
    });
  }
}
