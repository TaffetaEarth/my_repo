package Figures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class TriangleTest{

  @Test void testTriangle(){
    Triangle trian = new Triangle(9);
    assertEquals(27, trian.getPerimeter());
  }
}
