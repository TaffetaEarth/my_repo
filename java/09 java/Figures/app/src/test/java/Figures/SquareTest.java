package Figures;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class SquareTest{
  @Test void testSquare(){
    Square sq = new Square(5);
    assertEquals(20, sq.getPerimeter());
  }
}
