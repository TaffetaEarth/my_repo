package matrix.vectors;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class VectorsTest{
  @Test void vectorScalarMultiplyTest(){
    Vector vector = new Vector(0, 0, 3, 3);
    Vector result = new Vector();
    result = vector.vectorScalarMultiply(5);
    assertTrue(result.startx == 0 && result.starty == 0 && result.endx == 15 && result.endy == 15);
  }

  @Test void vectorScalarDividingTest(){
    Vector vector = new Vector(0, 0, 5, 5);
    Vector result = new Vector();
    result = vector.vectorScalarDividing(5);
    assertTrue(result.startx == 0 && result.starty == 0 && result.endx == 1 && result.endy == 1);
  }

  @Test void vectorSumTest(){
    Vector vector1 = new Vector(0, 0, 3, 3);
    Vector vector2 = new Vector(0, 0, 5, 5);
    Vector result = vector1.vectorSum(vector2);
    assertTrue(result.startx == 0 && result.starty == 0 && result.endx == 8 && result.endy == 8);
  }
}
