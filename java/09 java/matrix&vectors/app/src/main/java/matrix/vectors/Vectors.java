package matrix.vectors;
import java.util.Random;


class Vector{
  public int startx;
  public int starty;
  public int endx;
  public int endy;

  Vector(){}

  Vector(int startx, int starty, int endx, int endy){
    this.startx = startx;
    this.starty = starty;
    this.endx = endx;
    this.endy = endy;
  }


  public void vectorGen(){
    this.startx = new Random().nextInt(100);
    this.starty = new Random().nextInt(100);
    this.endx = new Random().nextInt(100);
    this.endy = new Random().nextInt(100);
  }

  public Vector vectorScalarMultiply(int coef){
    Vector vector = new Vector();
    vector.endx = (this.endx - this.startx) * coef;
    vector.endy = (this.endy - this.starty) * coef;
    return vector;
  }

  public Vector vectorScalarDividing(int coef){
    Vector vector = new Vector();
    vector.endx = (this.endx - this.startx) / coef;
    vector.endy = (this.endy - this.starty) / coef;
    return vector;
  }

  public Vector vectorSum(Vector vector){
    Vector result = new Vector();
    result.startx = this.startx;
    result.starty = this.starty;
    result.endx = this.endx + (vector.endx - vector.startx);
    result.endy = this.endy + (vector.endy - vector.starty);
    return result;
  }

  public double getLength(){
    return Math.sqrt(Math.pow(this.endx - this.startx, 2) + Math.pow(this.endy - this.starty, 2));
  }
}
