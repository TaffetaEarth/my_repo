/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package matrix.vectors;
import java.util.Random;
import java.io.*;

class Matrix implements Serializable{
  public int n;
  public int m;
  public int[][] content;
  public int threadsnum;
  public int[] slices;
  private static final long serialVersionUID = 1L;

  Matrix(){}

  Matrix(int n, int m){
    this.m = m;
    this.n = n;
    this.content = new int[n][m];
  }

  Matrix(int[][] content) throws IncorrectMatrixDimensionsException{
    this.m = content.length;
    this.n = content[0].length;
    for (int i = 0; i < this.m; i++) {
      if (content[i].length != this.n) {
        throw new IncorrectMatrixDimensionsException();
      }
    }
    this.content = content;
  }

  Matrix(int m, int n, int[][] content) throws IncorrectMatrixDimensionsException{
    this.m = m;
    this.n = n;
    for (int i = 0; i < this.m; i++) {
      if (content[i].length != n) {
        throw new IncorrectMatrixDimensionsException();
      }
    }
    this.content = content;
  }

  public void matrixGeneration(){
    int[][] matrix = new int[this.n][this.m];
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        matrix[i][j] = new Random().nextInt(10);
      }
    }
    this.content = matrix;
  }

  public void matrixOutput(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        System.out.print(this.content[i][j] + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }

  public void matrixOutputStarsSky(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        String space = "";
        for (int k = 0; k < 2 + new Random().nextInt(3); k++) {
          space += " ";
        }
        System.out.print(this.content[i][j] + space);
        try {
          Thread.sleep(2);
        } catch (Exception e) {}
      }
    }
  }

  public void matrixDividing(int threadsnum){
    this.threadsnum = threadsnum;
    Matrix[] resultarr = new Matrix[threadsnum];
    int n = this.n;
    int j = 0;
    int[] lenArr = new int[threadsnum];
    while (j < n){
      for (int i = 0; i < threadsnum; i++) {
        if (j < n){
          lenArr[i]++;
          j++;
        }
      }
    }
    this.slices = lenArr;
  }

  public Matrix matrixAddition(Matrix m1) throws IncorrectMatrixDimensionsException{
    if (this.n != m1.n || this.m != m1.m){
      throw new IncorrectMatrixDimensionsException();
    }
      Matrix result = new Matrix(this.n, this.m);
      for(int i = 0; i < this.n; i++){
        for (int j = 0; j < this.m; j++){
          result.content[i][j] = this.content[i][j] + m1.content[i][j];
        }
      }
      return result;
  }

  public Matrix matrixSubtraction(Matrix m1) throws IncorrectMatrixDimensionsException{
    if (this.n != m1.n || this.m != m1.m){
      throw new IncorrectMatrixDimensionsException();
    }
      Matrix result = new Matrix(this.n, this.m);
      for(int i = 0; i < this.n; i++){
        for (int j = 0; j < this.m; j++){
          result.content[i][j] = this.content[i][j] - m1.content[i][j];
        }
      }
      return result;
  }

  public Matrix matrixMultiplication(Matrix m2) throws IncorrectMatrixDimensionsException{
    if (this.m != m2.n || this.n != m2.m){
      throw new IncorrectMatrixDimensionsException();
    }
    Matrix result = new Matrix(this.m, m2.n);
    for (int i = 0; i < this.m; i++){
      for (int k = 0; k < this.n; k++){
        for (int j = 0; j < m2.n; j++){
        result.content[i][j] += this.content[i][k] * m2.content[k][j];
        }
      }
    }
    return result;
  }

  public int[][] getContent(){
    return this.content;
  }
}
