package parallelprogramming.tobaccosmokers;

public enum TableStatuses {
    FILLED, EMPTY, NOT_FILLED
}
