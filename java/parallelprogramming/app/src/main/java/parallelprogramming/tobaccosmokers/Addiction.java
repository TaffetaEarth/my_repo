package parallelprogramming.tobaccosmokers;

import java.util.Random;

public class Addiction implements Runnable {

    private static final int MIN_ADDICTION_TIME = 5000;
    private static final int RANGE_ADDICTION_TIME = 5000;

    Addiction(Smoker[] smokers) {
        this.smokers = smokers;
    }

    Smoker[] smokers;

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(MIN_ADDICTION_TIME + new Random().nextInt(RANGE_ADDICTION_TIME));
                Smoker smoker = smokers[new Random().nextInt(smokers.length)];
                if (smoker.getSmoked()) {
                    System.out.println("Smoker " + smoker.getName() + " now is addictive!");
                    smoker.becomeAddict();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
