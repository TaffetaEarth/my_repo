package parallelprogramming.tobaccosmokers;

public enum SmokerStatuses {
    WAITING, SMOKING, RESOURCE_GIVEN
}
