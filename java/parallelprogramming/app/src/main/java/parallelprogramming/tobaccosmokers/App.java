package parallelprogramming.tobaccosmokers;

public class App {
    public static void main(String[] args) {
        Table table = new Table();

        Smoker[] smokers = {new Smoker("Tobacco", Resources.TOBACCO, table),
                            new Smoker("Fire", Resources.FIRE, table),
                            new Smoker("Paper", Resources.PAPER, table)};


        Barman barman = new Barman(smokers, table);

        for (Smoker smoker: smokers){
            smoker.setBarman(barman);
        }

        Addiction addiction = new Addiction(smokers);

        new Thread(barman).start();
        new Thread(addiction).start();
        for (Smoker smoker: smokers){
            new Thread(smoker).start();
        }
    }
}
