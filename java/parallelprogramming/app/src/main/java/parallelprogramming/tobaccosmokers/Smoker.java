package parallelprogramming.tobaccosmokers;

import java.util.Random;

public class Smoker implements Runnable {
    Barman barman;

    String name;

    static final int MIN_SMOKING_TIME = 5000;
    static final int RANGE_SMOKING_TIME = 5000;

    SmokerStatuses status = SmokerStatuses.WAITING;

    Resources resource;

    Table table;

    Boolean smoked = false;


    public Smoker(String name, Resources resource, Table table) {
        this.name = name;
        this.resource = resource;
        this.table = table;
    }

    public void setBarman(Barman barman) {
        this.barman = barman;
    }

    public String getName() {
        return name;
    }

    public synchronized SmokerStatuses getStatus() {
        return status;
    }

    public synchronized void setStatus(SmokerStatuses status) {
        this.status = status;
    }

    public synchronized Boolean getSmoked() {
        return smoked;
    }

    public synchronized Resources getResource() {
        return resource;
    }

    public synchronized void becomeAddict() {
        this.smoked = false;
    }

    public void goSmoke() throws InterruptedException {
        this.table.clear();
        this.status = SmokerStatuses.SMOKING;
        System.out.println(name + " is smoking!");
        this.barman.resetAllStatuses();
        Thread.sleep(MIN_SMOKING_TIME + new Random().nextInt(RANGE_SMOKING_TIME));
        this.status = SmokerStatuses.WAITING;
        this.smoked = true;
        System.out.println(name + " returned and waiting!");
    }

    public void run() {
        System.out.println("Smoker " + this.name + " is ready to smoke!");
        while (true) {
            if (table.getStatus() == TableStatuses.FILLED) {
                if (!this.smoked && table.checkResources(this.resource)) {
                    try {
                        goSmoke();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }
}
