package parallelprogramming.tobaccosmokers;

import java.util.Random;

public class Barman implements Runnable {

    Smoker[] smokers;

    Table table;

    public static final int MIN_DELAY_TIME = 3000;
    public static final int RANGE_DELAY_TIME = 5000;

    public Barman(Smoker[] smokers, Table table) {
        this.smokers = smokers;
        this.table = table;
    }


    public void takeResource(Smoker smoker) {
        Resources[] resources = this.table.getResources();
        for (int i = 0; i < resources.length; i++) {
            if (resources[i] == null) {
                resources[i] = smoker.getResource();
                System.out.println(smoker.getResource() + " is on the table!");
                smoker.setStatus(SmokerStatuses.RESOURCE_GIVEN);
                if (i == resources.length - 1) {
                    table.setStatus(TableStatuses.FILLED);
                } else {
                    table.setStatus(TableStatuses.NOT_FILLED);
                }
                return;
            }
        }
    }

    public synchronized void resetAllStatuses(){
        for (Smoker smoker: smokers){
            if (smoker.getStatus() != SmokerStatuses.SMOKING) {
                smoker.setStatus(SmokerStatuses.WAITING);
            }
        }
    }

    @Override
    public void run() {
        System.out.println("Barman is on the work!");
        while (true) {
            try {
                Thread.sleep(MIN_DELAY_TIME + new Random().nextInt(RANGE_DELAY_TIME));
                Smoker smoker = smokers[new Random().nextInt(smokers.length)];
                if (smoker.getStatus() == SmokerStatuses.WAITING) {
                    takeResource(smoker);
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
