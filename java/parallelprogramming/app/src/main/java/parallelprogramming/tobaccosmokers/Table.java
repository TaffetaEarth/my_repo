package parallelprogramming.tobaccosmokers;

public class Table {

    public TableStatuses status = TableStatuses.EMPTY;

    Resources[] resources = new Resources[2];

    public synchronized void setStatus(TableStatuses status) {
        this.status = status;
    }

    public synchronized TableStatuses getStatus() {
        return status;
    }

    public synchronized Resources[] getResources() {
        return resources;
    }

    public synchronized boolean checkResources(Resources resource) {
        for (Resources currentResource : resources) {
            if (currentResource == null || currentResource == resource) {
                return false;
            }
        }
        return true;
    }
    
    public void clear(){
        for (int i = 0; i < resources.length; i++) {
            resources[i] = null;
        }
    }
}



