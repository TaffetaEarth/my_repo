package parallelprogramming.readersandwriters;

public class App {
    public static void main(String[] args) {
        Book book = new Book();
        Thread[] threads = {
                new Thread(new Writer(book, args[0])),
                new Thread(new Writer(book, args[0])),
                new Thread(new Reader(book, args[1])),
                new Thread(new Reader(book, args[1]))
        };

        for (Thread thread : threads) {
            thread.start();
        }
    }
}
