package parallelprogramming.readersandwriters;

import java.util.concurrent.Semaphore;

/**
 * класс книги
 */
public class Book {
    /**
     * семафор разрешения на взаимодействие с книгой
     */
    public Semaphore permit = new Semaphore(1, true);

    /**
     * поле для данных
     */
    private String data = "";

    /**
     * метод, возвращающий данные
     * @return
     */
    public String getData() {
        return this.data;
    }

    /**
     * метод, записывающий данные в поле
     * @param data - данные, которые нужно записать
     * @throws InterruptedException
     */
    public void put(String data) throws InterruptedException {
        this.permit.acquire();
        this.data = "";
        for (char sym : data.toCharArray()) {
            this.data += sym;
        }
        System.out.println("Data is " + data);
        notifyAll();
        this.permit.release();
    }

    /**
     * метод, возвращающий данные из поля книги
     * @return - данные, которые хранятся в книге
     * @throws InterruptedException
     */
    public String take() throws InterruptedException {
        if (this.permit.availablePermits() == 0 || this.data == null) {
            wait();
        }
        return this.data;
    }
}