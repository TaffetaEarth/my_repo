package parallelprogramming.readersandwriters;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Random;

/**
 * класс писателя
 */
public class Writer implements Runnable {

    /**
     * текущее время
     */
    private Long currentTime = null;

    /**
     * файл для логирования
     */
    static final String FILE_OUTPUT_NAME = "./reader.txt";
    /**
     * файл для логирования
     */
    String FILE_OUTPUT_NAME_DISPERSION;
    /**
     * минимальная задержка
     */
    static final int LATENCY = 10;
    /**
     * диапазон задержки
     */
    static final int LATENCY_BOUNDARY = 5;

    /**
     * книга, в которую мы будем писать данные
     */
    private Book book;

    /**
     * данные для записи
     */
    private String[] someInfo = {"Hello...Hello...Hello...",
            "Is there anybody in there?",
            "Just nod if you can hear me",
            "Is there anyone home?",
            "Come on now",
            "I hear you're feeling down",
            "Well I can ease your pain",
            "Get you on your feet again",
            "Relax",
            "I'll need some information first",
            "Just the basic facts",
            "Can you show me where it hurts?",
            "There is no pain you are receding",
            "A distant ship smoke on the horizon",
            "You are only coming through in waves",
            "Your lips move but I can't hear what you're saying",
            "When I was a child I had a fever",
            "My hands felt just like two balloons",
            "Now I've got that feeling once again",
            "I can't explain you would not understand",
            "This is not how I am",
            "I have become comfortably numb",
            "I have become comfortably numb",
            "Okay (okay, okay, okay)",
            "Just a little pinprick",
            "There'll be no more, ah",
            "But you may feel a little sick",
            "Can you stand up?",
            "I do believe it's working, good",
            "That'll keep you going through the show",
            "Come on it's time to go",
            "There is no pain you are receding",
            "A distant ship, smoke on the horizon",
            "You are only coming through in waves",
            "Your lips move but I can't hear what you're saying",
            "When I was a child",
            "I caught a fleeting glimpse",
            "Out of the corner of my eye",
            "I turned to look but it was gone",
            "I cannot put my finger on it now",
            "The child is grown",
            "The dream is gone",
            "I have become comfortably numb"};


    Writer(Book book, String file) {
        this.book = book;
        this.FILE_OUTPUT_NAME_DISPERSION = file;
    }

    /**
     * метод для записи потока
     */
    public void run() {
        while (true) {
            try {
                if (currentTime != null) {
                    long c = (new Date().getTime() - currentTime);
                    writeInFile(FILE_OUTPUT_NAME, LATENCY, LATENCY + LATENCY_BOUNDARY, c);
                    System.out.println("Ожидаемая задержка - " + LATENCY + " - " + (LATENCY + LATENCY_BOUNDARY) + ", реальная задержка - " + c);
                    writeInFileDispersion(FILE_OUTPUT_NAME_DISPERSION, c);
                }
                currentTime = new Date().getTime();
                System.out.println("Start writing");
                this.book.put(this.someInfo[new Random().nextInt(someInfo.length)]);
                System.out.println("Finish writing");
                Thread.sleep(LATENCY + new Random().nextInt(LATENCY_BOUNDARY));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    /**
     * метод для записи в файл лога дисперсии
     * @param fileName - имя файла
     * @param c - реальная задержка
     */
    public static synchronized void writeInFileDispersion(String fileName, long c) {
        try (FileWriter fileWriter = new FileWriter(fileName, true)) {
            String text = c + "\n";
            Files.write(Paths.get(fileName), text.getBytes(), StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    /**
     * метод для записи в файл лога
     * @param fileName - имя файла
     * @param a - минимальная задержка
     * @param b - диапазон задержки
     * @param c - реальная задержка
     */
    public static void writeInFile(String fileName, int a, int b, long c) {
        try (FileWriter fileWriter = new FileWriter(fileName, true)) {
            String text = "Ожидаемая задержка - " + a + " - " + b + ", реальная задержка - " + c + "\n";
            Files.write(Paths.get(fileName), text.getBytes(), StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

