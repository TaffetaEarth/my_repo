package parallelprogramming.readersandwriters;

import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Random;

/**
 * класс читателя
 */
public class Reader implements Runnable {

    /**
     * текущее время
     */
    private Long currentTime = null;
    /**
     * файл для логирования
     */
    static final String FILE_OUTPUT_NAME = "./reader.txt";
    /**
     * файл для логирования в формате дисперсии
     */
    String FILE_OUTPUT_NAME_DISPERSION;
    /**
     * минимальная задержка
     */
    static final int LATENCY = 10;
    /**
     * диапазон задержки
     */
    static final int LATENCY_BOUNDARY = 5;

    /**
     * книга, из которой мы будем читать данные
     */
    private Book book;

    Reader(Book book, String file) {
        this.book = book;
        this.FILE_OUTPUT_NAME_DISPERSION = file;
    }

    /**
     * метод для запуска потока
     */
    public void run() {
        String message = " ";
        while (message != "") {
            try {
                if (currentTime != null) {
                    long c = (new Date().getTime() - currentTime);
                    writeInFile(FILE_OUTPUT_NAME, LATENCY, LATENCY + LATENCY_BOUNDARY, c);
                    System.out.println("Ожидаемая задержка - " + LATENCY + " - " + (LATENCY + LATENCY_BOUNDARY) + ", реальная задержка - " + c);
                    writeInFileDispersion(FILE_OUTPUT_NAME_DISPERSION, c);
                }
                currentTime = new Date().getTime();
                Thread.sleep(LATENCY + new Random().nextInt(LATENCY_BOUNDARY));
                System.out.println("Start reading");
                message = this.book.take();
                System.out.println("I've read " + message);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * метод для записи в файл лога
     * @param fileName - имя файла
     * @param a - минимальная задержка
     * @param b - диапазон задержки
     * @param c - реальная задержка
     */
    public static synchronized void writeInFile(String fileName, int a, int b, long c) {
        try (FileWriter fileWriter = new FileWriter(fileName, true)) {
            String text = "Ожидаемая задержка - " + a + " - " + b + ", реальная задержка - " + c + "\n";
            Files.write(Paths.get(fileName), text.getBytes(), StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * метод для записи в файл лога дисперсии
     * @param fileName - имя файла
     * @param c - реальная задержка
     */
    public static synchronized void writeInFileDispersion(String fileName, long c) {
        try (FileWriter fileWriter = new FileWriter(fileName, true)) {
            String text = c + "\n";
            Files.write(Paths.get(fileName), text.getBytes(), StandardOpenOption.APPEND);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}