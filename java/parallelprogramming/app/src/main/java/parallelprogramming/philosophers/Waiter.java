package parallelprogramming.philosophers;

import java.util.concurrent.Semaphore;

/**
 * класс официанта
 */
public class Waiter {

    Waiter(int forkCount) {
        this.forkCount = new Semaphore(forkCount, true);
    }
    /**
     * доступное количество вилок
     */
    public Semaphore forkCount;
    /**
     * метод, запрашивающий разрешение на использование вилок
     * @throws InterruptedException
     */
    public void takePermit() throws InterruptedException {
        this.forkCount.acquire(2);
    }
    /**
     * метод, освобождающий вилки
     */
    public void cleanFork() {
        this.forkCount.release(2);
    }
}
