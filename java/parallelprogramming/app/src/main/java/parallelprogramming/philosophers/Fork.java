package parallelprogramming.philosophers;

import java.util.concurrent.Semaphore;

/**
 * класс вилки
 */
public class Fork {
    /**
     * фактор доступности вилки
     */
    Semaphore available = new Semaphore(1, true);
    /**
     * метод чтобы взять вилку со стола
     * @throws InterruptedException
     */
    public boolean take() throws InterruptedException {
        this.available.acquire();
        return true;
    }
    /**
     * метод чтобы положить вилку на стол
     */
    public void put() {
        this.available.release();
    }

    /**
     * проверка на доступность вилки
     */
    public boolean isAvailable(){
        return this.available.availablePermits() > 0;
    }
}
