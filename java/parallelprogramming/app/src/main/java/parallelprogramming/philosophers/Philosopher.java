package parallelprogramming.philosophers;

import java.util.Random;

/**
 * класс философа
 */
public class Philosopher implements Runnable {
    /**
     * статус философа
     */
    public PhilosophersEnum status = PhilosophersEnum.THINKING;
    /**
     * имя философа
     */
    public String name;
    /**
     * минимальное время еды
     */
    private static final int MIN_EATING_TIME = 5000;
    /**
     * диапазон времени еды
     */
    private static final int EATING_TIME_DIAPASON = 3000;
    /**
     * вилка "слева"
     */
    public Fork lfork;
    /**
     * вилка "справа"
     */
    public Fork rfork;
    /**
     * официант, обслуживающий стол
     */
    public Waiter waiter;

    Philosopher(Fork lfork, Fork rfork, Waiter waiter) {
        this.lfork = lfork;
        this.rfork = rfork;
        this.waiter = waiter;
    }

    /**
     * метод для запуска потока
     */
    @Override
    public void run() {
        while (true) {
            try {
                this.waiter.takePermit();
                if (this.lfork.isAvailable() && this.rfork.isAvailable()) {
                    this.lfork.take();
                    this.rfork.take();
                    System.out.println(name + " forks taken!");
                    this.status = PhilosophersEnum.EATING;
                    Thread.sleep(MIN_EATING_TIME + new Random().nextInt(EATING_TIME_DIAPASON));
                    System.out.println(name + " philosopher's lunch end!");
                    this.status = PhilosophersEnum.THINKING;
                    this.rfork.put();
                    this.lfork.put();
                }
                this.waiter.cleanFork();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
