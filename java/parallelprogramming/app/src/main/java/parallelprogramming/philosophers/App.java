package parallelprogramming.philosophers;

public class App {

    static final int FORK_AMOUNT = 5;

    public static void main(String[] args) {

        Waiter waiter = new Waiter(FORK_AMOUNT);

        Fork[] forks = new Fork[FORK_AMOUNT];

        for (int i = 0; i < FORK_AMOUNT; i++){
            forks[i] = new Fork();
        }

        Philosopher[] philosophers = new Philosopher[FORK_AMOUNT];

        for (int i = 0; i < FORK_AMOUNT - 1; i++){
            philosophers[i] = new Philosopher(forks[i], forks[i+1], waiter);
            philosophers[i].name = "" + (i + 1);
        }

        philosophers[FORK_AMOUNT - 1] = new Philosopher(forks[FORK_AMOUNT - 1], forks[0], waiter);
        philosophers[FORK_AMOUNT - 1].name = "" + FORK_AMOUNT;


        for (Philosopher philosopher: philosophers){
            new Thread(philosopher).start();
        }
    }
}
