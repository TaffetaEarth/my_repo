package parallelprogramming.hairdresser;

import parallelprogramming.hairdresser.statuses.*;

/**
 * класс клиента
 */
public class Client implements Runnable {
    /**
     * имя клиента
     */
    String name;

    /**
     * комната ожидания на рабочем месте парикмахера
     */
    private final Room room;

    /**
     * рабочее место парикмахера
     */
    private final Chair chair;

    Client(Room room, Chair chair) {
        this.chair = chair;
        this.room = room;
    }

    /**
     * метод, устанавливающий имя
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * метод, отправляющий клиента на стрижку
     */
    public synchronized void goToChair() {
        room.leave();
        try {
            chair.occupy();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * метод, отпускающий клиента домой
     */
    public synchronized void leaveChair() {
        chair.leave();
    }


    /**
     * метод для запуска потока
     */
    public void run() {
            try {
                if (room.takePlace()) {
                    System.out.println(this.name + " is in the room!");
                    chair.takePlaceInQueue();
                    System.out.println(this.name + " is on the chair!");
                    goToChair();
                    System.out.println(this.name + " is trimmed and going home!");
                    leaveChair();
                } else {
                    System.out.println("No free seats! " + this.name + " has gone!");
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
        }
    }
}

