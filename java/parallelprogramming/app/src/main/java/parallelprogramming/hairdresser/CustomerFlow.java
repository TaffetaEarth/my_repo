package parallelprogramming.hairdresser;

import java.util.Random;

/**
 * класс создания постоянного потока клиентов
 */
public class CustomerFlow implements Runnable {

    /**
     * комната, в которую приходят клиенты
     */
    Room room;
    /**
     * рабочее место парикмахера, к которому приходят клиенты
     */
    Chair chair;

    /**
     * минимальное время между приходами клиентов
     */
    private static final int LATENCY = 5000;
    /**
     * диапазон времени между приходами клиентов
     */
    private static final int LATENCY_BOUNDARY = 5000;

    /**
     * @param room  - рабочее место парикмахера, к которому приходят клиенты
     * @param chair комната, в которую приходят клиенты
     */
    CustomerFlow(Room room, Chair chair) {
        this.room = room;
        this.chair = chair;
    }

    /**
     * метод для запуска потока
     */
    public void run() {
        int i = 0;
        while (true) {
            try {
                Thread.sleep(LATENCY + new Random().nextInt(LATENCY_BOUNDARY));
                i++;
                Client client = new Client(room, chair);
                client.setName("Client " + i);
                new Thread(client).start();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
