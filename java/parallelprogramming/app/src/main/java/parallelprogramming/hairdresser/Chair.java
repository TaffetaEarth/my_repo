package parallelprogramming.hairdresser;

import parallelprogramming.hairdresser.statuses.*;

import java.util.concurrent.Semaphore;

/**
 * класс стула
 */
public class Chair {
    /**
     * парикмахер, работающий на этом стуле
     */
    public Hairdresser hairdresser;
    /**
     * место на стуле
     */
    Semaphore place = new Semaphore(1, true);
    /**
     * статус стула
     */
    public ChairStatuses status = ChairStatuses.FREE;

    /**
     * метод для назначения парикмахера
     *
     * @param hairdresser - парикмахер, которого нужно назначить
     */
    public void setHairdresser(Hairdresser hairdresser) {
        this.hairdresser = hairdresser;
    }



    /**
     * метод, чтобы парикмахер спал на стуле
     */
    public synchronized void sleepOnChair() {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * метод ожидания парикмахером клиента для завершения действия
     */
    public synchronized void waitForClient() {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * метод для оповещения клиента на стуле
     */
    public synchronized void ring() {
        notifyAll();
    }
}
