package parallelprogramming.hairdresser;



public class App {
    public static void main(String[] args) {
        Room room = new Room(10);
        Chair chair = new Chair();
        Hairdresser hairdresser = new Hairdresser(room, chair);
        new Thread(new CustomerFlow(room, chair)).start();
        chair.setHairdresser(hairdresser);
        new Thread(hairdresser).start();
    }
}
