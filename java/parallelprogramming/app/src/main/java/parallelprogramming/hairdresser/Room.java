package parallelprogramming.hairdresser;

import java.util.concurrent.Semaphore;

/**
 * класс комнаты
 */
public class Room {

    /**
     * количество мест в комнате
     */
    public Semaphore placesInRoom;

    public int capacity;

    /**
     * @param capacity - количество мест в комнате
     */
    public Room(int capacity) {
        this.capacity = capacity;
        this.placesInRoom = new Semaphore(capacity, true);
    }

    /**
     * метод, позволяющий клиенту занят место в комнате, если там есть свободные места
     * @return - boolean есть ли в комнате свободные места
     * @throws InterruptedException
     */
    public boolean takePlace() throws InterruptedException {
        return placesInRoom.tryAcquire();
    }

    /**
     * проверка комнаты на пустоту
     * @return - boolean пустая ли комната
     */
    public synchronized boolean isEmpty() {
        return !(this.placesInRoom.availablePermits() < capacity);
    }
}
