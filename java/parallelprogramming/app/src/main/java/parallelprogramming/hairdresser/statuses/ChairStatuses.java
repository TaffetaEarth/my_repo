package parallelprogramming.hairdresser.statuses;

public enum ChairStatuses {
    OCCUPIED, FREE
}
