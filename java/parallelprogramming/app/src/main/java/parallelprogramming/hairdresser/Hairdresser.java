package parallelprogramming.hairdresser;

import parallelprogramming.hairdresser.statuses.*;

import java.util.Random;

/**
 * класс парикмахера
 */
public class Hairdresser implements Runnable {

    public Hairdresser(Room room, Chair chair) {
        this.room = room;
        this.chair = chair;
    }

    /**
     * минимальное рабочее время
     */
    private static final int MIN_WORK_TIME = 5000;
    /**
     * диапазон рабочего времени
     */
    private static final int WORK_TIME_RANGE = 5000;
    /**
     * статус парикмахера
     */
    public HairdresserStatuses status;
    /**
     * комната ожидания на рабочем месте парикмахера
     */
    public Room room;
    /**
     * рабочее место парикмахера
     */
    public Chair chair;

    /**
     * метод для запуска потока
     */
    public void run() {
        System.out.println("Hairdresser is on the work!");
        while (true) {
            try {
                if (room.isEmpty() && chair.status == ChairStatuses.FREE) {
                    this.status = HairdresserStatuses.SLEEPING;
                    System.out.println("Hairdresser is sleeping!");
                    this.chair.sleepOnChair();
                } else {
                    if (this.status == HairdresserStatuses.WORKING) {
                        chair.waitForClient();
                    }
                    System.out.println("Hairdresser start working!");
                    this.status = HairdresserStatuses.WORKING;
                    Thread.sleep(MIN_WORK_TIME + new Random().nextInt(WORK_TIME_RANGE));
                    System.out.println("Hairdresser finish working!");
                    chair.ring();
                    chair.waitForClient();
                    chair.place.release();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
