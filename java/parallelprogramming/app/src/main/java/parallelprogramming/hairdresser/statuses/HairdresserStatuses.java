package parallelprogramming.hairdresser.statuses;

public enum HairdresserStatuses {
    WORKING, SLEEPING
}
