package ru.sirius.main.program;
import java.util.Date;
import java.io.*;

class SerializeMain{
  public static void main(String[] args) {
    Matrix m1 = new Matrix(10, 10);
    m1.matrixGeneration();
    System.out.println("до:");
    m1.matrixOutput();
    FileOutputStream outputStream = null;
    ObjectOutputStream objectOutputStream = null;
    try{
      outputStream = new FileOutputStream("save.txt");
      objectOutputStream = new ObjectOutputStream(outputStream);
      objectOutputStream.writeObject(m1);
    } catch (IOException e){}
    finally {
      try {
        objectOutputStream.close();
      } catch (IOException e) {}
    }
    System.out.println("после:");
    FileInputStream inputStream = null;
    ObjectInputStream objectInputStream = null;
    try{
      inputStream = new FileInputStream("save.txt");
      objectInputStream = new ObjectInputStream(inputStream);
      Matrix resultmatrix = (Matrix) objectInputStream.readObject();
      resultmatrix.matrixOutput();
      // System.out.println(resultmatrix.m);
      // System.out.println(resultmatrix.n);
    } catch (IOException|ClassNotFoundException e){
      System.out.println("Ex");
    }
    finally {
      try {
        objectOutputStream.close();
      } catch (IOException e) {}
    }
  }
}
