package ru.sirius.main.program;


class ThreadSumMatrix extends Thread{
  public Matrix m1;
  public Matrix m2;
  public Matrix result;
  public int i;
  public int size;

  ThreadSumMatrix(Matrix m1, Matrix m2, int i, int size){
    this.m1 = m1;
    this.m2 = m2;
    this.i = i;
    this.size = size;
    result = new Matrix(this.size, m1.m);
  }

  public void run(){
    int l = 0;
    for (int k = this.i; k < this.i + this.size; k++) {
      for (int j = 0; j < m1.m; j++) {
        result.content[l][j] = m1.content[k][j] + m2.content[k][j];
      }
      l++;
    }
  }
}
