package ru.sirius.main.program;
import java.util.Random;
import java.io.*;

class Matrix implements Serializable{
  public int n;
  public int m;
  public int[][] content;
  public int threadsnum;
  public int[] slices;
  private static final long serialVersionUID = 1L;

  Matrix(){}

  Matrix(int n, int m){
    this.n = n;
    this.m = m;
    this.content = new int[n][m];
  }

  Matrix(int[][] content){
    this.n = content.length;
    this.m = content[0].length;
    this.content = content;
  }

  Matrix(int n, int m, int[][] content){
    this.n = n;
    this.m = m;
    this.content = content;
  }

  public void matrixGeneration(){
    int[][] matrix = new int[this.n][this.m];
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        matrix[i][j] = new Random().nextInt(10);
      }
    }
    this.content = matrix;
  }

  public void matrixOutput(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        System.out.print(this.content[i][j] + "\t");
      }
      System.out.println();
    }
    System.out.println();
  }

  public void matrixOutputStarsSky(){
    for (int i = 0; i < this.n; i++){
      for (int j = 0; j < this.m; j++){
        String space = "";
        for (int k = 0; k < 2 + new Random().nextInt(3); k++) {
          space += " ";
        }
        System.out.print(this.content[i][j] + space);
        try {
          Thread.sleep(2);
        } catch (Exception e) {}
      }
    }
  }

  public void matrixDividing(int threadsnum){
    this.threadsnum = threadsnum;
    Matrix[] resultarr = new Matrix[threadsnum];
    int n = this.n;
    int j = 0;
    int[] lenArr = new int[threadsnum];
    while (j < n){
      for (int i = 0; i < threadsnum; i++) {
        if (j < n){
          lenArr[i]++;
          j++;
        }
      }
    }
    this.slices = lenArr;
  }

  public int[][] getContent(){
    return this.content;
  }

  // public Matrix MatrixSum(Matrix matrix, int threadsnum){
  //   Matrix matrixsumm = new Matrix(this.n, this.m);
  //   ThreadSumMatrix[] threadsarray = new ThreadSumMatrix[threadsnum];
  //   for (int i = 0; i < threadsnum; i++) {
  //     threadsarray[i] = new ThreadSumMatrix(this.fragments[i], matrix.fragments[i]);
  //     try{
  //       threadsarray[i].start();
  //     } catch (Exception e) {}
  //   }
  //   for (int i = 0; i < threadsnum; i++){
  //     try{
  //       threadsarray[i].join();
  //     } catch (Exception e) {}
  //   }
  //   int i = 0;
  //   int fragnum = 0;
  //   while (i < matrixsumm.n) {
  //     for (int j = 0; j < threadsarray[fragnum].result.n; j++) {
  //         matrixsumm.content[i] = threadsarray[fragnum].result.content[j];
  //       i++;
  //     }
  //     fragnum++;
  //   }
  //   return matrixsumm;
  // }
}
