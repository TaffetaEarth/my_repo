package ru.sirius.main.program;
import java.util.Date;
import java.io.*;

class Main{
  public static void main(String[] args) {
    int k = 12;
    Matrix m1 = new Matrix(10000, 10000);
    FileOutputStream outputStream = null;
    ObjectOutputStream objectOutputStream = null;
    try{
      outputStream = new FileOutputStream("save.txt");
      objectOutputStream = new ObjectOutputStream(outputStream);
      objectOutputStream.writeObject(m1);
    } catch (IOException e){}
    finally {
      try {
        objectOutputStream.close();
      } catch (IOException e) {}
    }
    Matrix m2 = new Matrix(10000, 10000);
    Matrix matrixsumm = new Matrix(m1.n, m1.m);
    m1.matrixGeneration();
    m2.matrixGeneration();
    long time1 = new Date().getTime();
    m1.matrixDividing(k);
    m2.matrixDividing(k);
    ThreadSumMatrix[] threadsarray = new ThreadSumMatrix[k];
    int startindex = 0;
    for (int i = 0; i < k; i++) {
      threadsarray[i] = new ThreadSumMatrix(m1, m2, startindex, m1.slices[i]);
      startindex += m1.slices[i];
      try{
        threadsarray[i].start();
      } catch (Exception e) {}
    }
    for (int i = 0; i < k; i++){
      try{
        threadsarray[i].join();
      } catch (Exception e) {}
    }
    int i = 0;
    int fragnum = 0;
    while (i < matrixsumm.n) {
      for (int j = 0; j < threadsarray[fragnum].result.n; j++) {
          matrixsumm.content[i] = threadsarray[fragnum].result.content[j];
        i++;
      }
      fragnum++;
    }
    long time2 = new Date().getTime();
    matrixsumm.matrixOutput();
    System.out.println(time2 - time1);
  }
}
