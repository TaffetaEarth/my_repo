package query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Holder {
    public String name;

    public String phone;
    public static ArrayList<Holder> holders = new ArrayList<Holder>();

    Holder(String name, String phone){
        this.name = name;
        this.phone = phone;

    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }


    public static boolean validatePresence(String name, String phone){
        return name != "" && phone != "";
    }

    public void create(String name, String phone){
        if (validatePresence(name, phone)) {
            try {
                Connect.execute("insert into Holders (name, phone) values (" + name + ", " + phone + ")");
            } catch (SQLException e){}
        } else {
            System.out.println("Invalidate values!");
        }
    }

    public void save(){
        if (validatePresence(this.name, this.phone)){
            try {
                Connect.execute("insert into Holders (name, phone) values (" + name + ", " + phone + ")");
            } catch (SQLException e){};
        } else {
            System.out.println("Invalidate values!");
        }
    }

    public static ArrayList<Holder> getAll(){
       holders.clear();
        try {
            String query = "select * from Holder";
            ResultSet rs = Connect.execute(query);
            while (rs.next()) {
                String name = rs.getString("name");
                String phone = rs.getString("phone");
                holders.add(new Holder(name, phone));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return holders;
    }

    public static ArrayList<Holder> getByPhone(String phone){
        holders.clear();
        try {
            String query = "select * from Holder where phone =" + phone;
            ResultSet rs = Connect.execute(query);
            while (rs == null){
                rs = Connect.execute(query);
            }
            while (rs.next()) {
                String name = rs.getString("name");
                String phoneNumber = rs.getString("phone");
                holders.add(new Holder(name, phoneNumber));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return holders;
    }


    public static ArrayList<Holder> getByName(String holderName){
        holders.clear();
        try {
            String query = "select * from Holder where name =" + holderName;
            ResultSet rs = Connect.execute(query);
            while (rs.next()) {
                String name = rs.getString("name");
                String phoneNumber = rs.getString("phone");
                holders.add(new Holder(name, phoneNumber));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return holders;
    }
}
