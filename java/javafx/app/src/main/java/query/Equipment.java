package query;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Equipment {
    String title;
    String color;
    static ArrayList<Equipment> equipments = new ArrayList<Equipment>();


    Equipment(String name, String phone){
        this.title = name;
        this.color = phone;

    }

    public static ArrayList<Equipment> getAll(){
        equipments.clear();
        try {
            String query = "select * from Equipment";
            ResultSet rs = Connect.execute(query);
            while (rs.next()) {
                String title = rs.getString("title");
                String color = rs.getString("color");
                equipments.add(new Equipment(title, color));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return equipments;
    }

    public static ArrayList<Equipment> getByTitle(String title){
        equipments.clear();
        try {
            String query = "select * from Holder where title =" + title;
            ResultSet rs = Connect.execute(query);
            while (rs.next()) {
                String eqTitle = rs.getString("title");
                String color = rs.getString("color");
                equipments.add(new Equipment(eqTitle, color));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return equipments;
    }


    public static ArrayList<Equipment> getByColor(String color){
        equipments.clear();
        try {
            String query = "select * from Holder where color =" + color;
            ResultSet rs = Connect.execute(query);
            while (rs.next()) {
                String title = rs.getString("title");
                String eqColor = rs.getString("color");
                equipments.add(new Equipment(title, eqColor));
            }
        } catch (Exception e){
            System.out.println(e);
        }
        return equipments;
    }
}

