package firstapp;

import javafx.geometry.Insets;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import query.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.*;

import java.util.ArrayList;

public class HelloFx extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Connect.getConnection();
        ArrayList<Holder> holders = Holder.getAll();

        TableView<Holder> tblHolders = new TableView<>();
        tblHolders.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        VBox.setVgrow(tblHolders, Priority.ALWAYS );

        TableColumn<Holder, String> holderName = new TableColumn<>("Holder's name");
        TableColumn<Holder, String> holderPhone = new TableColumn<>("Holder's phone");

        holderName.setCellValueFactory( new PropertyValueFactory<Holder, String>("name") );
        holderPhone.setCellValueFactory( new PropertyValueFactory<Holder, String>("phone") );

        tblHolders.getColumns().addAll(holderName, holderPhone);

        for (int i = 0; i < holders.size(); i++) {
            tblHolders.getItems().add(holders.get(i));
        }

        VBox vbox = new VBox( tblHolders );
        vbox.setPadding( new Insets(10) );
        vbox.setSpacing( 10 );

        Scene scene = new Scene(vbox);

        primaryStage.setTitle("TableSelectApp");
        primaryStage.setScene( scene );
        primaryStage.setHeight( 1080 );
        primaryStage.setWidth( 1920 );
        primaryStage.show();
    }

    public static void main(String[] args) {

        launch(args);
    }
}
