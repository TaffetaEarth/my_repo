const Pool = require("pg").Pool;
const fs = require("fs");
const { createClient } = require('@clickhouse/client')


const masterPool = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST ?? 'localhost',
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
});

const slavePool = new Pool({
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST ?? 'localhost',
    database: process.env.POSTGRES_DB,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_SLAVE_PORT,
});

const click_client = createClient({
    host: 'http://clickhouse:8123',
    database: 'postgres_repl',
    username: process.env.CLICKHOUSE_USER ?? 'clickhouse',
    password: process.env.CLICKHOUSE_PASSWORD ?? 'clickhouse',
})

async function findClickSearchArtistByLabelCountry() {
    const resultSet = await click_client.query({
        query: `select artists.title
                from artists
                         join labels_to_artists lta on artists.id = lta.artist_id
                         join labels l on lta.label_id = l.id
                WHERE l.country = 'USA';`,
        format: 'JSONEachRow',
    })
    return await resultSet.json()
}

async function findClickByMember() {
    const resultSet = await click_client.query({
        query: `select * from artists where sells > 220000000`,
        format: 'JSONEachRow',
    })
    return await resultSet.json()
}



async function findByMember(member) {
    try {
        const result = await masterPool.query(
            'SELECT title, members FROM artists WHERE members @> ARRAY [$1]',
            [member]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function findByAlbum(year) {
    try {
        const result = await masterPool.query(
            'SELECT title, discography ->> $1 FROM artists WHERE discography ? $1',
            [year]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function findArtist(id) {
    try {
        const result = await masterPool.query(
            'SELECT * FROM artists WHERE id = $1',
            [id]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function findArtistInSLave(id) {
    try {
        const result = await slavePool.query(
            'SELECT * FROM artists WHERE id = $1',
            [id]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function dangerous(id) {
    try {
        const query = `SELECT * FROM labels WHERE title = '${id}'`
        const result = await masterPool.query(query)
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function findLabel(id) {
    try {
        const result = await masterPool.query(
            'SELECT * FROM labels WHERE id = $1',
            [id]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function findSong(id) {
    try {
        const result = await masterPool.query(
            'SELECT * FROM songs WHERE id = $1',
            [id]
        )
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function addSong(title, duration, song_text, hit) {
    try {
        const result = await masterPool.query(
            'INSERT INTO songs (title, duration, song_text, hit) VALUES' +
            '($1, $2, $3, $4, $5)', [title, duration, song_text, hit]
        )
    } catch (error) {
        console.log(error)
    }
}

async function deleteSong(id) {
    try {
        const result = await masterPool.query(
            'DELETE FROM songs WHERE id = $1', [id])
    } catch (error) {
        console.log(error)
    }
}

async function updateSong(id, artist_id) {
    try {
        const result = await masterPool.query(
            'UPDATE songs SET artist_id = $1 WHERE id = $2', [artist_id, id])
    } catch (error) {
        console.log(error)
    }
}

async function allArtists() {
    try {
        const result = await masterPool.query(
            'SELECT * FROM artists')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function allLabels() {
    try {
        const result = await masterPool.query(
            'SELECT * FROM labels')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

async function allSongs() {
    try {
        const result = await masterPool.query(
            'SELECT * FROM songs')
        return result.rows
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    findClickByMember,
    findClickSearchArtistByLabelCountry,
    findByAlbum,
    findLabel,
    findArtist,
    findArtistInSLave,
    findByMember,
    findSong,
    allArtists,
    allLabels,
    allSongs,
    addSong,
    deleteSong,
    updateSong,
    dangerous
}

