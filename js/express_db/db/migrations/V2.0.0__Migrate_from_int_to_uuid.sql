begin;

drop trigger if exists update_songs_table on songs;

drop function if exists refresh_greatest_songs;

drop materialized view greatest_songs;

create extension if not exists "uuid-ossp";

alter table labels_to_artists
    drop constraint labels_to_artists_artist_id_fkey;

alter table labels_to_artists
    drop constraint labels_to_artists_label_id_fkey;

alter table labels_to_artists
    rename column artist_id to old_artist_id;

alter table labels_to_artists
    rename column label_id to old_label_id;

alter table labels_to_artists
    add column artist_id uuid;

alter table labels_to_artists
    add column label_id uuid;


alter table songs
    drop constraint songs_artist_id_fkey;

alter table songs
    rename column artist_id to old_artist_id;

alter table songs
    add column artist_id uuid;

alter table songs
    drop constraint songs_pkey;

alter table songs
    rename column id to old_id;

alter table songs
    add column id uuid default uuid_generate_v4();

alter table artists
    drop constraint artists_pkey;

alter table artists
    rename column id to old_id;

alter table artists
    add column id uuid default uuid_generate_v4();

alter table labels
    drop constraint labels_pkey;

alter table labels
    rename column id to old_id;

alter table labels
    add column id uuid default uuid_generate_v4();

do
$$
    declare
        row record;
    begin
        for row in select * from artists
            loop
                update labels_to_artists set artist_id = row.id where old_artist_id = row.old_id;
                update songs set artist_id = row.id where old_artist_id = row.old_id;
            end loop;
        for row in select * from labels
            loop
                update labels_to_artists set label_id = row.id where old_label_id = row.old_id;
            end loop;
    end
$$;

alter table artists
    drop column old_id;
alter table artists
    add primary key (id);

alter table labels
    drop column old_id;
alter table labels
    add primary key (id);

alter table songs
    drop column old_id;
alter table songs
    add primary key (id);

alter table labels_to_artists
    drop column old_artist_id;
alter table labels_to_artists
    drop column old_label_id;
alter table labels_to_artists
    add constraint fk_artist_id foreign key (artist_id) references artists;
alter table labels_to_artists
    add constraint fk_label_id foreign key (label_id) references labels;
alter table labels_to_artists
    alter column artist_id set not null;
alter table labels_to_artists
    add primary key (artist_id, label_id);

alter table songs
    drop column old_artist_id;

alter table songs
    add constraint songs_artist_id_fkey foreign key (artist_id) references artists;

CREATE MATERIALIZED VIEW greatest_songs as
SELECT DISTINCT ON (song, artist) s.title as song, s.duration, a.title as artist, a.members, l.country, l.title
FROM songs s
         JOIN artists a on a.id = s.artist_id
         JOIN labels_to_artists l_a on a.id = l_a.artist_id
         JOIN labels l on l_a.label_id = l.id
WHERE hit = true
  AND (a.founded <= 2000 OR a.founded IS NULL);

CREATE UNIQUE INDEX index_greatest_songs_on_song_and_artist ON greatest_songs USING btree (song, artist);

REFRESH MATERIALIZED VIEW CONCURRENTLY greatest_songs;

create function refresh_greatest_songs()
    returns trigger as
$$
begin
    refresh materialized view concurrently greatest_songs;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_songs_table
    after insert or update or delete
    on songs
    for each row
execute function refresh_greatest_songs();

select cron.schedule('refresh_greatest_songs', '*/30 * * * *',
                     $$ refresh materialized view concurrently greatest_songs $$);

commit;