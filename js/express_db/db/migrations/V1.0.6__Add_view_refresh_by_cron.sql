drop function if exists refresh_greatest_songs;
create function refresh_greatest_songs()
    returns trigger as
$$
begin
    refresh materialized view concurrently greatest_songs;

    return new;
end;
$$
    language 'plpgsql';

create trigger update_songs_table
    after insert or update or delete
    on songs
    for each row
execute function refresh_greatest_songs();

create extension pg_cron;

select cron.schedule('refresh_greatest_songs', '*/30 * * * *',
                     $$ refresh materialized view concurrently greatest_songs $$);