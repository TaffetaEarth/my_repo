INSERT INTO artists (title, members, founded, break_up, sells, country, discography)
VALUES ('AC/DC',
        '{"guitarist": ["Angus Young", "Malcolm Young"],"drummer": "Phil Rudd", "bassist": "Cliff Williams","singer": "Brian Johnson"}',
        1973,
        2017,
        200000000,
        'Australia',
        ARRAY [
            '{ "year": "1975", "title": ["High Voltage", "T.N.T."]  }',
            '{ "year": "1976", "title": "Dirty Deeds Done Dirt Cheap" }',
            '{ "year": "1977", "title": "Let There Be Rock" }',
            '{ "year": "1978", "title": "Powerage" }',
            '{ "year": "1979", "title": "Highway to Hell" }',
            '{ "year": "1980", "title": "Back in Black" }',
            '{ "year": "1981", "title": "For Those About to Rock We Salute You" }',
            '{ "year": "1983", "title": "Flick of the Switch" }',
            '{ "year": "1985", "title": "Fly on the Wall" }',
            '{ "year": "1988", "title": "Blow Up Your Video" }',
            '{ "year": "1990", "title": "The Razors Edge" }',
            '{ "year": "1995", "title": "Ballbreaker" }',
            '{ "year": "2000", "title": "Stiff Upper Lip" }',
            '{ "year": "2008", "title": "Black Ice" }',
            '{ "year": "2014", "title": "Rock or Bust" }',
            '{ "year": "2020", "title": "Power Up" }']::json[]),
       ('Led Zeppelin',
        '{"guitarist": "Jimmy Page","drummer": "John Bonham", "bassist": "John Paul Jones","singer": "Robert Plant"}',
        1968,
        1980,
        250000000,
        'England',
        ARRAY [
            '{ "year": "1969", "title": ["Led Zeppelin", "Led Zeppelin II"] }',
            '{ "year": "1970", "title": "Led Zeppelin III" }',
            '{ "year": "1971", "title": "Untitled album" }',
            '{ "year": "1973", "title": "Houses of the Holy" }',
            '{ "year": "1975", "title": "Physical Graffiti" }',
            '{ "year": "1976", "title": "Presence" }',
            '{ "year": "1979", "title": "In Through the Out Door" }',
            '{ "year": "1982", "title": "Coda" }']::json[]);

INSERT INTO songs (title, artist_id, duration, song_text, hit)
VALUES ('Stairway to Heaven',
        2,
        481,
        'There''s a lady who''s sure
        All that glitters is gold
        And she''s buying a stairway to heaven
        When she gets there she knows
        If the stores are all closed
        With a word she can get what she came for
        Ooh ooh ooh ooh ooh
        And she''s buying a stairway to heaven
        There''s a sign on the wall
        But she wants to be sure
        ''Cause you know, sometimes words have two meanings
        In a tree by the brook
        There''s a songbird who sings
        Sometimes all of our thoughts are misgiven
        Ooh, it makes me wonder
        Ooh, it makes me wonder
        There''s a feeling I get
        When I look to the west
        And my spirit is crying for leaving
        In my thoughts I have seen
        Rings of smoke through the trees
        And the voices of those who stand looking
        Ooh, it makes me wonder
        Ooh, it really makes me wonder
        And it''s whispered that soon
        If we all call the tune
        Then the piper will lead us to reason
        And a new day will dawn
        For those who stand long
        And the forests will echo with laughter
        Oh whoa-whoa-whoa, oh-oh
        If there''s a bustle in your hedgerow, dont be alarmed now
        It''s just a spring clean for the May Queen
        Yes, there are two paths you can go by, but in the long run
        And there''s still time to change the road you''re on
        And it makes me wonder
        Oh, whoa
        Your head is humming and it won''t go
        In case you don''t know
        The piper''s calling you to join him
        Dear lady, can you hear the wind blow?
        And did you know
        Your stairway lies on the whispering wind?
        And as we wind on down the road
        Our shadows taller than our soul
        There walks a lady we all know
        Who shines white light and wants to show
        How everything still turns to gold
        And if you listen very hard
        The tune will come to you at last
        When all are one and one is all, yeah
        To be a rock and not to roll
        And she''s buying a stairway to heaven',
        true),
       ('Moby Dick',
        2,
        261,
        '-',
        false),
       ('Highway to Hell',
        1,
        208,
        'Livin'' easy
                    Lovin'' free
                    Season ticket on a one way ride
                    Askin'' nothin''
                    Leave me be
                    Takin'' everythin'' in my stride
                    Don''t need reason
                    Don''t need rhyme
                    Ain''t nothin'' that I''d rather do
                    Goin'' down
                    Party time
                    My friends are gonna be there too
                    I''m on the highway to hell
                    On the highway to hell
                    Highway to hell
                    I''m on the highway to hell
                    No stop signs
                    Speed limit
                    Nobody''s gonna slow me down
                    Like a wheel
                    Gonna spin it
                    Nobody''s gonna mess me around
                    Hey satan
                    Payin'' my dues
                    Playin'' in a rockin'' band
                    Hey mumma
                    Look at me
                    I''m on the way to the promised land
                    I''m on the highway to hell
                    Highway to hell
                    I''m on the highway to hell
                    Highway to hell
                    Don''t stop me
                    I''m on the highway to hell
                    On the highway to hell
                    Highway to hell
                    I''m on the highway to hell
                    (Highway to hell) I''m on the highway to hell
                    (Highway to hell) highway to hell
                    (Highway to hell) highway to hell
                    (Highway to hell)
                    And I''m goin'' down
                    All the way
                    I''m on the highway to hell',
        true);

INSERT INTO labels (title, founded, country, company)
VALUES ('Atlantic Records',
        1947,
        'USA',
        'Warner Music Group'),
       ('Swan Song Records',
        1974,
        'UK',
        '-'),
       ('East West Records',
        1955,
        'USA',
        'Independent Label Group');

INSERT INTO labels_to_artists
VALUES (1, 2),
       (2, 2),
       (2, 1),
       (3, 1);
