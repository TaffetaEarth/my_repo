CREATE TABLE artists (
    id int GENERATED ALWAYS AS IDENTITY primary key,
    title text unique,
    members jsonb,
    discography jsonb[],
    founded int not null,
    country text
);

create index artist_members on artists using gin (members);

set enable_seqscan = false;

create index artist_discography on artists using gin (discography);