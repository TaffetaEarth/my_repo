CREATE MATERIALIZED VIEW greatest_songs as
SELECT DISTINCT ON (song, artist) s.title as song, s.duration, a.title as artist, a.members, l.country, l.title
FROM songs s
JOIN artists a on a.id = s.artist_id
JOIN labels_to_artists l_a on a.id = l_a.artist_id
JOIN labels l on l_a.label_id = l.id
WHERE hit = true
AND (a.founded <= 2000 OR a.founded IS NULL);

CREATE UNIQUE INDEX index_greatest_songs_on_song_and_artist ON greatest_songs USING btree (song, artist);

REFRESH MATERIALIZED VIEW concurrently greatest_songs;

