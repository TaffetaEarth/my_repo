drop table if exists artists;
drop table if exists songs;

create table artists
(
    title   text,
    "members.soloist" Array(text),
    "members.guitarist" Array(text),
    "members.drummer" Array(text),
    "members.bassist" Array(text),
    founded int,
    country text
) engine = ReplacingMergeTree() order by (title);

create table songs
(
    title        text,
    artist_title text,
    duration     int,
    song_text    text,
    hit          boolean
) engine = Memory();

insert into artists (title, "members.soloist", "members.guitarist", "members.drummer", "members.bassist", founded,
                     country)
values ('Led Zeppelin',
        ['Robert Plant'],
        ['Jimmy Page'],
        ['John Bonham'],
        ['John Paul Jones'],
        1968,
        'England'),
       ('AC/DC',
        ['Brian Johnson'],
        ['Angus Young'],
        ['Phil Rudd'],
        ['Cliff Williams'],
        1973,
        'Australia');

insert into songs (title, artist_title, duration, song_text, hit)
values ('Stairway to Heaven',
        'Led Zeppelin',
        481,
        'There''s a lady who''s sure...',
        true),
       ('Moby Dick',
        'Led Zeppelin',
        261,
        '-',
        false),
       ('Highway to Hell',
        'AC/DC',
        208,
        'Livin'' easy...',
        true
       );

select *
from artists
where has("members.soloist", 'Brian Johnson');


select max(duration)
from songs join artists a on songs.artist_title = a.title
WHERE has("members.guitarist", 'Jimmy Page');

insert into artists (title, "members.soloist", "members.guitarist", "members.drummer", "members.bassist", founded,
                     country)
values ('AC/DC',
        ['Brian Johnson'],
        ['Malcolm Young'],
        ['Phil Rudd'],
        ['Cliff Williams'],
        1973,
        'Australia');

select * from artists final;
