require('dotenv').config()
const {Client} = require('@elastic/elasticsearch')
const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const db = require('./db_worker')
const app = express();


const client = new Client({node: 'http://elasticsearch:9200'})

app.use(bodyParser.json())

app.get('/', (req, res) => {
    res.send('Express app');
});

app.get('/click/by_member', async (req, res) => {
    try {
        const result = await db.findClickByMember()
        res.json(result[0])
    } catch (e) {
        console.log(e)
        return res.status(400)
    }
});

app.get('/click/by_song_duration', async (req, res) => {
    try {
        const result = await db.findClickSearchArtistByLabelCountry()
        res.json(result)
    } catch (e) {
        console.log(e)
        return res.status(400)
    }
});

app.get('/elastic/term', async (req, res) => {
    try {
        const year = await req.query.year
        console.log(year)
        const result = await client.search({
            index: 'artists',
            "query": {
                "term": {
                    "founded": `${year}`
                }
            }
        });
        return res.json(result.hits.hits)
    } catch (e) {
        console.log(e)
        return res.status(422)
    }
});

app.get('/elastic/artists/aggregation', async (req, res) => {
    const result = await client.search({
        index: 'artists',
        body: {
            aggs: {
                max_sells: {
                    max: {
                        field: "sells"
                    }
                },
                min_sells: {
                    min: {
                        field: "sells"
                    }
                },
                avg_sells: {
                    weighted_avg: {
                        value: {
                            field: "sells"
                        },
                        weight: {
                            field: "founded"
                        }
                    }
                },
                total_sells: {
                    sum: {
                        script: "return doc['sells'].value / (doc['break_up'].value - doc['founded'].value)"
                    }
                },
                price_extended_stats: {
                    extended_stats: {
                        field: "sells"
                    }
                }
            },
            "size": 0
        }
    });
    return res.json(result);
});

app.get('/elastic/songs/autocomplete', async (req, res) => {

    try {
        res.sendFile(path.join(__dirname, '/static/autocomplete.html'));
    } catch (error) {
        console.log(error)
    }
});

app.get('/autocomplete', async (req, res) => {
    const title = await req.query.title
    const result = await client.search({
        index: "songs",
        query: {
            "match": {
                "title": {
                    "query": title,
                    "analyzer": "my_ngram_analyzer"
                }
            }
        }
    });
    return res.json(result.hits.hits.map(song => song._source.title))
});

app.get('/array', async (req, res) => {
    const {member} = req.params;
    const result = await db.findByMember(member)
    res.json(result[0])
});

app.get('/json', async (req, res) => {
    const {year} = req.params;
    const result = await db.findByAlbum(year)
    res.json(result[0])
});

app.get('/artist/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.findArtist(id)
    res.json(result[0])
});

app.get('/slave/artist/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.findArtistInSLave(id)
    res.json(result[0])
});

app.get('/label/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.findLabel(id)
    res.json(result[0])
});

app.get('/song/:id', async (req, res) => {
    const id = parseInt(req.params.id)
    const result = await db.findSong(id)
    res.json(result[0])
});

app.get('/artists', async (req, res) => {
    const result = await db.allArtists()
    res.json(result)
});

app.get('/songs', async (req, res) => {
    const result = await db.allSongs()
    res.json(result)
});

app.post('/injection', async (req, res) => {
    const {id} = req.body
    const inj = await db.dangerous(id)
    res.status(200).json(inj)
});

app.get('/labels', async (req, res) => {
    const result = await db.allLabels()
    res.json(result)
});

app.post('/add_song', async (req, res) => {
    const {title, duration, song_text, hit} = req.body
    await db.addSong(title, duration, song_text, hit)
    res.status(200).json({message: "Successfully added", status: 200})
});

app.post('/song/:id/delete', async (req, res) => {
    const id = parseInt(req.params.id)
    await db.deleteSong(id)
    res.status(200).json({message: "Successfully deleted", status: 200})
});

app.post('/song/:id/update', async (req, res) => {
    const {artist_id} = req.body
    const id = parseInt(req.params.id)
    await db.updateSong(id, artist_id)
    res.status(200).json({message: "Successfully updated", status: 200})
});

app.listen(process.env.APP_PORT);

console.log(`Running on http://localhost:${process.env.APP_PORT}`);