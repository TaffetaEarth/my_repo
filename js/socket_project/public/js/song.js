const socket = io()

const root = document.querySelector('#root')
root.insertAdjacentHTML(
    'beforebegin',
    `<button class="add_button">Добавить песню</button>`
)
const addButton = document.querySelector('.add_button')
addButton.addEventListener('click', () => {
    console.log('add button pressed')
    socket.emit('add_song')
})

function deleteSong(id) {
    const blockId = `eq_${id}`
    document.querySelector(`#${blockId}`).remove()
    socket.emit('remove_song', id)
}

function addSong(id, title, artist_id, duration, song_text) {
    const blockId = `eq_${id}`

    root.insertAdjacentHTML(
        'beforebegin',
        `<div id=${blockId}>
            <span>Название:</span>
            <input class="title" value="${title ?? ''}">
            <span>ID исполнителя:</span>
            <input class="artist_id" value="${artist_id ?? "-"}">
            <span>Продолжительность:</span>
            <input class="duration" value="${duration ?? 0}">
            <span>Текст песни:</span>
            <input class="song_text" value="${song_text ?? '-'}">
            <button class="save_button btn btn-primary">сохранить</button>
            <button class="delete_button btn btn-primary">удалить</button>
        </div>`
    )

    const saveButton = document.querySelector(`#${blockId} .save_button`)
    saveButton.addEventListener('click', () => {
        const title = document.querySelector(`#${blockId} .title`).value
        const artist_id = document.querySelector(`#${blockId} .artist_id`).value
        const duration = document.querySelector(`#${blockId} .duration`).value
        const song_text = document.querySelector(`#${blockId} .song_text`).value

        socket.emit('update_song', { id, title, artist_id, duration, song_text })
    })

    const deleteButton = document.querySelector(`#${blockId} .delete_button`)
    deleteButton.addEventListener('click', () => deleteSong(id))
}

async function loadSong() {
    const response = await fetch('/song/all')

    if (response.ok) {
        const songs = await response.json()
        console.log(songs)

        for (const song of songs) addSong(song.id, song.title, song.artist_id, song.duration, song.song_text)
    } else {
        console.error(response.status)
    }
}

loadSong()

function updateSong(sng) {
    const blockId = `eq_${sng.id}`
    document.querySelector(`#${blockId} .title`).value = sng.title
    document.querySelector(`#${blockId} .artist_id`).value = sng.artist_id
    document.querySelector(`#${blockId} .duration`).value = sng.duration
    document.querySelector(`#${blockId} .song_text`).value = sng.song_text
}

socket.on('song_updated', updateSong)
socket.on('song_removed', id => deleteSong(id))
socket.on('song_added', id => {
    console.log(`song added with id = ${id}`)
    addSong(id)
})
