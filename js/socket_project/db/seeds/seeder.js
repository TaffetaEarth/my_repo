const fs = require("fs");
const Pool = require("pg").Pool;

const seedPath = './V2.0.0__Insert_data.sql'

const pool = new Pool({
    user: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    host: process.env.POSTGRES_HOST ?? 'localhost',
    port: process.env.POSTGRES_PORT,
    database: process.env.POSTGRES_DB
})


async function seed() {
    try {
        let sql = fs.readFileSync(seedPath, "utf8");
        await pool.query(sql)
    } catch (error) {
        console.log(error)
    }
}
