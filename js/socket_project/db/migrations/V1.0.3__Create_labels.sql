CREATE TABLE labels (
    id int GENERATED ALWAYS AS IDENTITY primary key,
    title text unique,
    founded integer,
    country text,
    company text
);

