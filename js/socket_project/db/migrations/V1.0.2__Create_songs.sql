CREATE TABLE songs (
    id int GENERATED ALWAYS AS IDENTITY primary key,
    title text unique,
    artist_id int references artists,
    duration int default 0,
    song_text text,
    hit boolean default false
);

CREATE INDEX index_songs_on_title_artist_id_by_hit ON songs (title, artist_id, hit);