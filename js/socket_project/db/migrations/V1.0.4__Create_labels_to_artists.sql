CREATE TABLE labels_to_artists(
    label_id int references labels,
    artist_id int references artists,
    primary key (label_id, artist_id)
);


