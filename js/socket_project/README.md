Установить node.js версии >= 16. Это можно сделать через [nvm](https://github.com/nvm-sh/nvm)

Поставить пакеты `npm ci`

Запуск проекта: `npm run watch`

Установка типов: `npm i @types/express @types/node @types/pg ts-node tslib typescript @types/cors`
