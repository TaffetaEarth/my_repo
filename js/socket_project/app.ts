import { config } from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import { Pool } from 'pg';
import { Server } from 'socket.io';

config();

const app = express();
const server = http.createServer(app);
const io = new Server(server);

app.use(express.static(`${__dirname}/public`));

const pool = new Pool({
  user: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  host: process.env.POSTGRES_HOST ?? 'localhost',
  port: Number(process.env.POSTGRES_PORT),
  database: process.env.POSTGRES_DB,
});

interface Song {
  id: number;
  title: string;
  artist_id: number;
  duration: number;
  song_text: string;
  hit: boolean;
}

io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on('add_song', async () => {
    try {
      console.log('add_song emitted');

      const query = `
                insert into songs (title)
                values ('')
                returning id
            `;

      const queryRes = await pool.query<Song>(query);

      if (queryRes.rows.length > 0) io.emit('song_added', queryRes.rows[0].id);
    } catch (err) {
      console.error(err);
    }
  });

  socket.on('update_song', async (eq: Song) => {
    try {
      console.log(`update_song emitted: song = `, eq);

      const query = `
            update songs
            set title = $1, artist_id = $2, duration = $3,
                song_text = $4
            where id = $5
            returning id
            `;

      const queryRes = await pool.query<Song>(query, [
        eq.title,
        eq.artist_id,
        eq.duration,
        eq.song_text,
        eq.id,
      ]);
      if (queryRes.rows.length > 0) io.emit('song_updated', eq);
    } catch (err) {
      console.error(err);
    }
  });

  socket.on('remove_song', async (id: number) => {
    try {
      console.log(`remove_song emitted: id = ${id}`);

      const query = `
            delete from songs
            where id = $1
            returning id
            `;

      const queryRes = await pool.query<{ id: number }>(query, [id]);

      if (queryRes.rows.length > 0) io.emit('song_removed', id);
    } catch (err) {
      console.error(err);
    }
  });

  socket.on('disconnect', () => {
    console.log('user disconnected');
  });
});

app.get('/song', (_, res) => {
  try {
    res.sendFile(`${__dirname}/public/song.html`);
  } catch (err) {
    console.error(err);
  }
});

app.get('/song/all', async (_, res) => {
  try {
    const query = 'select * from songs order by id';
    const queryRes = await pool.query(query);

    res.json(queryRes.rows);
  } catch (err) {
    console.error(err);
  }
});

app.use(bodyParser.json());

const port = process.env.APP_PORT;
server.listen(port, () => {
  console.log(`app listening on http://localhost:${port}/`);
  console.log(`equipment page - http://localhost:${port}/song`);
});
